//=========================================================
// SharePic
//
// Custom Control + Demo Project created by FV iMAGINATION
// for CodeCanyon
// ========================================================


#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GADBannerView.h>

BOOL cameraPic;

@interface MainScreen : UIViewController
<
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
GADBannerViewDelegate
>

@property (weak, nonatomic) IBOutlet UIImageView *myImage;


// Labels
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cameraLabel;
@property (weak, nonatomic) IBOutlet UILabel *photoLibrarylabel;
@property (strong, nonatomic) GADBannerView *gAdBannerView;
@property (weak, nonatomic) IBOutlet UIButton *restorePurchaseBtn;


// Buttons
- (IBAction)cameraButt:(id)sender;
- (IBAction)photoLibraryButt:(id)sender;

@end
