//
//  ViewController.h
//  FilterX
//
//  Created by MacBook FV iMAGINATION on 17/07/14.
//  Copyright (c) 2014 FV iMAGINATION. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <iAd/iAd.h>
#import <AudioToolbox/AudioToolbox.h>
#import <GoogleMobileAds/GADBannerView.h>
#import <GoogleMobileAds/GADInterstitial.h>
#import "CRColorPicker.h"

enum MEDIA_TYPE
{
    MEDIA_TYPE_INVALID = 0,
    MEDIA_TYPE_PHOTO,
    MEDIA_TYPE_VIDEO,
};

enum BACKGROUND_MODE
{
    BACKGROUND_MODE_PATTERN = 0,
    BACKGROUND_MODE_COLOR,
};

@interface ImageEditor : UIViewController
<
UIScrollViewDelegate,
UIDocumentInteractionControllerDelegate,
ADBannerViewDelegate, GADBannerViewDelegate, GADInterstitialDelegate,
UIGestureRecognizerDelegate, CRColorPickerDelegate, UIAlertViewDelegate
>

// Image/Video properties
@property (nonatomic, assign)NSInteger mediaIndex;
@property (strong, nonatomic)NSURL* videoURL;
//@property (strong, nonatomic)GPUImageMovieWriter *movieWriter;

// Image passed from MainScreen
@property (weak, nonatomic) UIImage *imagePassed;

//Ad banners properties
@property (strong, nonatomic) ADBannerView *iAdBannerView;
@property (strong, nonatomic) GADBannerView *gAdBannerView;
@property (strong, nonatomic) GADInterstitial *gInterstitialView;

// Labels ========
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

// Images ========
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *borderImage;
@property (weak, nonatomic) IBOutlet UIImageView *filteredImageView;
@property (weak, nonatomic) IBOutlet UIImageView *watermarkImageView;
@property (weak, nonatomic) IBOutlet UISlider *blurSlider;

// ScrollView and PageControl for filtered image ==========
@property (weak, nonatomic) IBOutlet UIScrollView *filteredImagesScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageFilter;


// Buttons ========
@property (weak, nonatomic) IBOutlet UIButton *backOutlet;
@property (weak, nonatomic) IBOutlet UIButton *shareOutlet;
@property (weak, nonatomic) IBOutlet UIButton *moveScaleOutlet;
@property (weak, nonatomic) IBOutlet UIButton *blurCheckBtn;
@property (weak, nonatomic) IBOutlet UIButton *filterPackOut1;
@property (weak, nonatomic) IBOutlet UIButton *filterPackOut2;
@property (weak, nonatomic) IBOutlet UIButton *filterPackOut3;
@property (weak, nonatomic) IBOutlet UIButton *borderPattBtn;
@property (weak, nonatomic) IBOutlet UIButton *borderColorBtn;
@property (weak, nonatomic) IBOutlet UIButton *shadowBtn;
@property (weak, nonatomic) IBOutlet UIButton *removeWatermarkButton;
@property (weak, nonatomic) IBOutlet UIButton *removeAdsButton;
@property (weak, nonatomic) IBOutlet UIView *bottomBannerView;

// Borders Views ========
@property (weak, nonatomic) IBOutlet UIView *bordersContainerView;
@property (weak, nonatomic) IBOutlet UIScrollView *bordersScrollView;
@property (weak, nonatomic) IBOutlet CRColorPicker *colorPickerView;

-(void) purchaseBlur;
@end
