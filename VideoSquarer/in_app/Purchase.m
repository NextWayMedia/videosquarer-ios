//
//  SharedPreference.m
//  Qditor_iPad
//
//  Created by common on 1/11/14.
//  Copyright (c) 2014 scn. All rights reserved.
//

#import "Purchase.h"

#define BLUR_PURCHASED                  @"blur_purchased"
#define REMOVEWATERMARK_PURCHASED       @"removewatermark_purchased"
#define REMOVEADS_PURCHASED             @"removeads_purchased"

#define IN_APP_PRODUCT_BLUR             @"enableblur"
#define IN_APP_PRODUCT_REMOVEADS        @"videosquarer_removeads"
#define IN_APP_PRODUCT_REMOVEWATERMARK  @"videosquarer_removewatermark"

@interface Purchase() <RMStoreReceiptVerificator>
{
    RMStore *store;
}
@end


@implementation Purchase
@synthesize isBlurPurchased;
@synthesize isRemoveAdsPurchased, isRemoveWatermarkPurchased;

+ (Purchase *)defaultPurchase
{
    static Purchase *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[Purchase class] alloc] initWithProductName:IN_APP_PRODUCT_BLUR];
    });
    return sharedInstance;
}

-(id)initWithProductName:(NSString*)productName
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSNumber * val = [userDefaults valueForKey:BLUR_PURCHASED];
    _isFirstTime = NO;
    
    if (val && val.boolValue) {
        isBlurPurchased = YES;
    }
    else
    {
        if(val == nil)
        {
            _isFirstTime = YES;
            [userDefaults setBool:isBlurPurchased forKey:BLUR_PURCHASED];
            [userDefaults synchronize];
        }
        else
            _isFirstTime = NO;
        isBlurPurchased = NO;
    }
    
    val = [userDefaults valueForKey:REMOVEWATERMARK_PURCHASED];
    if (val && val.boolValue) {
        isRemoveWatermarkPurchased = YES;
    }
    else
    {
        isRemoveWatermarkPurchased = NO;
    }

    val = [userDefaults valueForKey:REMOVEADS_PURCHASED];
    if (val && val.boolValue) {
        isRemoveAdsPurchased = YES;
    }
    else
    {
        isRemoveAdsPurchased = NO;
    }

    store = [RMStore defaultStore];
    
//    [store requestProducts:[NSSet setWithObject:productName]
//            success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
//                
//                for (int i=0; i< [products count]; i++) {
//                    
//                    SKProduct* product = ((SKProduct*)[products objectAtIndex:i]);
//                    if ([product.productIdentifier compare:IN_APP_PRODUCT] == NSOrderedSame) {
//                        
//                        NSLog(@"success request purchase");
//                        //g_isTrial = NO;
//                        //break;
//                    }
//                }
//                
//                for (int k=0; k<[invalidProductIdentifiers count]; k++) {
//                    
//                }
//        
//            } failure:^(NSError *error) {
//                NSLog(@"failure request purchase");
//            }];
    
    return self;
}

-(void) buyBlurCompletionHandler:(void (^)(BOOL isSuccess))successBlock
{
    [store requestProducts:[[NSSet alloc] initWithObjects:IN_APP_PRODUCT_BLUR, nil] success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
        [store addPayment:IN_APP_PRODUCT_BLUR
                   success:^(SKPaymentTransaction *transaction) {
                       NSLog(@"success request purchase");
                       isBlurPurchased = YES;
                       NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                       
                       NSNumber *val = [NSNumber numberWithBool:isBlurPurchased];
                       [userDefaults setObject:val forKey:BLUR_PURCHASED];
                       [userDefaults synchronize];
                       successBlock(YES);
                   }
         
                   failure:^(SKPaymentTransaction *transaction, NSError *error) {
                       NSLog(@"failure request purchase");
                       isBlurPurchased = NO;
                       NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                       
                       NSNumber *val = [NSNumber numberWithBool:isBlurPurchased];
                       [userDefaults setObject:val forKey:BLUR_PURCHASED];
                       [userDefaults synchronize];
                       successBlock(NO);
                   }];
    } failure:^(NSError *error) {
        NSLog(@"failure request purchase");
        isBlurPurchased = NO;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        NSNumber *val = [NSNumber numberWithBool:isBlurPurchased];
        [userDefaults setObject:val forKey:BLUR_PURCHASED];
        [userDefaults synchronize];
        successBlock(NO);
    }];
}

-(void) buyRemoveWatermarkCompletionHandler:(void (^)(BOOL))successBlock
{
    [store requestProducts:[[NSSet alloc] initWithObjects:IN_APP_PRODUCT_REMOVEWATERMARK, nil] success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
        [store addPayment:IN_APP_PRODUCT_REMOVEWATERMARK
                  success:^(SKPaymentTransaction *transaction) {
                      NSLog(@"success request purchase");
                      isRemoveWatermarkPurchased = YES;
                      NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                      
                      NSNumber *val = [NSNumber numberWithBool:isRemoveWatermarkPurchased];
                      [userDefaults setObject:val forKey:REMOVEWATERMARK_PURCHASED];
                      [userDefaults synchronize];
                      successBlock(YES);
                  }
         
                  failure:^(SKPaymentTransaction *transaction, NSError *error) {
                      NSLog(@"failure request purchase");
                      isRemoveWatermarkPurchased = NO;
                      NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                      
                      NSNumber *val = [NSNumber numberWithBool:isRemoveWatermarkPurchased];
                      [userDefaults setObject:val forKey:REMOVEWATERMARK_PURCHASED];
                      [userDefaults synchronize];
                      successBlock(NO);
                  }];
    } failure:^(NSError *error) {
        NSLog(@"failure request purchase");
        isRemoveWatermarkPurchased = NO;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        NSNumber *val = [NSNumber numberWithBool:isRemoveWatermarkPurchased];
        [userDefaults setObject:val forKey:REMOVEWATERMARK_PURCHASED];
        [userDefaults synchronize];
        successBlock(NO);
    }];
}

-(void) buyRemoveAdsCompletionHandler:(void (^)(BOOL))successBlock
{
    [store requestProducts:[[NSSet alloc] initWithObjects:IN_APP_PRODUCT_REMOVEADS, nil] success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
        [store addPayment:IN_APP_PRODUCT_REMOVEADS
                  success:^(SKPaymentTransaction *transaction) {
                      NSLog(@"success request purchase");
                      isRemoveAdsPurchased = YES;
                      NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                      
                      NSNumber *val = [NSNumber numberWithBool:isRemoveAdsPurchased];
                      [userDefaults setObject:val forKey:REMOVEADS_PURCHASED];
                      [userDefaults synchronize];
                      successBlock(YES);
                  }
         
                  failure:^(SKPaymentTransaction *transaction, NSError *error) {
                      NSLog(@"failure request purchase");
                      isRemoveAdsPurchased = NO;
                      NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                      
                      NSNumber *val = [NSNumber numberWithBool:isRemoveAdsPurchased];
                      [userDefaults setObject:val forKey:REMOVEADS_PURCHASED];
                      [userDefaults synchronize];
                      successBlock(NO);
                  }];
    } failure:^(NSError *error) {
        NSLog(@"failure request purchase");
        isRemoveAdsPurchased = NO;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        NSNumber *val = [NSNumber numberWithBool:isRemoveAdsPurchased];
        [userDefaults setObject:val forKey:REMOVEADS_PURCHASED];
        [userDefaults synchronize];
        successBlock(NO);
    }];
}

- (void)restoreTransactionsOnSuccess:(void (^)())successBlock
                             failure:(void (^)(NSError *error))failureBlock;
{
    [store restoreTransactionsOnSuccess:^(NSArray *purchasedList){

        for(int i = 0; i < purchasedList.count; i ++)
        {
            if([[purchasedList objectAtIndex:i] isEqualToString:IN_APP_PRODUCT_BLUR])
            {
                isBlurPurchased = YES;
                _isFirstTime = NO;
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                NSNumber *val = [NSNumber numberWithBool:isBlurPurchased];
                [userDefaults setObject:val forKey:BLUR_PURCHASED];
                [userDefaults synchronize];
            }
            else if([[purchasedList objectAtIndex:i] isEqualToString:IN_APP_PRODUCT_REMOVEWATERMARK])
            {
                isRemoveWatermarkPurchased = YES;
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                NSNumber *val = [NSNumber numberWithBool:isRemoveWatermarkPurchased];
                [userDefaults setObject:val forKey:REMOVEWATERMARK_PURCHASED];
                [userDefaults synchronize];
            }
            else if([[purchasedList objectAtIndex:i] isEqualToString:IN_APP_PRODUCT_REMOVEADS])
            {
                isRemoveAdsPurchased = YES;
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                NSNumber *val = [NSNumber numberWithBool:isRemoveAdsPurchased];
                [userDefaults setObject:val forKey:REMOVEADS_PURCHASED];
                [userDefaults synchronize];
            }
        }

        if (successBlock != nil)
            successBlock();
        
    }failure:^(NSError *error) {
        if(failureBlock != nil)
            failureBlock(error);
    }];
}

@end
