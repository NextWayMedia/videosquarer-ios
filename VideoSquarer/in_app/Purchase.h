//
//  SharedPreference.h
//  Qditor
//
//  Created by common on 1/11/14.
//  Copyright (c) 2014 scn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RMStore.h"


@interface Purchase : NSObject

-(id)initWithProductName:(NSString*)productName;
- (void)restoreTransactionsOnSuccess:(void (^)())successBlock
                             failure:(void (^)(NSError *error))failureBlock;

- (void) buyBlurCompletionHandler:(void (^)(BOOL isSuccess))successBlock;
- (void) buyRemoveWatermarkCompletionHandler:(void (^)(BOOL isSuccess))successBlock;
- (void) buyRemoveAdsCompletionHandler:(void (^)(BOOL isSuccess))successBlock;

+ (Purchase *)defaultPurchase;

@property (nonatomic, assign)BOOL isBlurPurchased;
@property (nonatomic, assign)BOOL isRemoveWatermarkPurchased;
@property (nonatomic, assign)BOOL isRemoveAdsPurchased;

@property (nonatomic, assign)BOOL isFirstTime;
@end
