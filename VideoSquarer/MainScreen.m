//=========================================================
// SharePic
//
// Custom Control + Demo Project created by FV iMAGINATION
// for CodeCanyon
// ========================================================


#import "MainScreen.h"
#import "ImageEditor.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import "Purchase.h"

@interface MainScreen ()
{
    NSInteger mediaIndex;
    NSURL *videoURL;
}
@end

@implementation MainScreen
@synthesize gAdBannerView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Purchase *purchase = [Purchase defaultPurchase];
    if(!purchase.isFirstTime)
        _restorePurchaseBtn.hidden = YES;

    if(!purchase.isRemoveAdsPurchased)
    {
        [self initgAdBanner];
        [self.gAdBannerView loadRequest:[GADRequest request]];
    }

    // Sets labels font
    _titleLabel.font = [UIFont fontWithName:@"What time is it?" size:25];
    _cameraLabel.font = [UIFont fontWithName:@"What time is it?" size:13];
    _photoLibrarylabel.font = [UIFont fontWithName:@"What time is it?" size:13];
    mediaIndex = MEDIA_TYPE_INVALID;
    
    videoURL = nil;
}

-(BOOL)prefersStatusBarHidden {
    return true;
}
// Prevent the StatusBar from showing up after picking an image
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:true];
}

- (IBAction)restorePurchaseBtn:(id)sender {
    Purchase *purchase = [Purchase defaultPurchase];
    [purchase restoreTransactionsOnSuccess:^{
        _restorePurchaseBtn.hidden = YES;
        
        if(purchase.isRemoveAdsPurchased)
        {
            [self.gAdBannerView removeFromSuperview];
        }

        NSString *strMessage = [NSString stringWithFormat:@"You have Successfully restored purchase."];
        UIAlertView *alertError =[[UIAlertView alloc] initWithTitle:@"Success !" message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertError show];

    } failure:^(NSError *error) {
        
    }];
}

- (IBAction)cameraButt:(id)sender {
    cameraPic = YES;
    
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            NSString *requiredMediaType1 = (__bridge NSString *)kUTTypeImage;
            NSString *requiredMediaType2 = (__bridge NSString *)kUTTypeMovie;
            
            picker.mediaTypes = [[NSArray alloc]
                                     initWithObjects:requiredMediaType1, requiredMediaType2, nil];

            picker.delegate = self;
            [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
            [self presentViewController:picker animated:YES completion:nil];
        }
}


- (IBAction)photoLibraryButt:(id)sender {
    cameraPic = NO;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        NSString *requiredMediaType1 = (__bridge NSString *)kUTTypeImage;
        NSString *requiredMediaType2 = (__bridge NSString *)kUTTypeMovie;
        
        picker.mediaTypes = [[NSArray alloc]
                             initWithObjects:requiredMediaType1, requiredMediaType2, nil];

        [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:picker animated:YES completion:nil];
    }
    
}

- (UIImage *)imageFromMovie:(NSURL *)movieURL atTime:(NSTimeInterval)time {

    AVAsset *asset = [[AVURLAsset alloc] initWithURL:movieURL options:nil];
    
    int durationSec = (int)CMTimeGetSeconds(asset.duration);

    if(durationSec < time)
        time = 0.0f;
#if 1
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    
    CMTime midpoint = CMTimeMakeWithSeconds(time, 1000);
    CMTime actualTime;
    
    CGImageRef preImage = [imageGenerator copyCGImageAtTime:midpoint actualTime:&actualTime error:NULL];
    NSLog(@"thumbnail size(%zu, %zu)", CGImageGetWidth(preImage), CGImageGetHeight(preImage));

    return [UIImage imageWithCGImage:preImage];
#else
    MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                   initWithContentURL:movieURL];
    mp.shouldAutoplay = NO;
    mp.initialPlaybackTime = time;
    mp.currentPlaybackTime = time;
    // get the thumbnail
    UIImage *thumbnail = [mp thumbnailImageAtTime:time
                                       timeOption:MPMovieTimeOptionNearestKeyFrame];
    // clean up the movie player
    [mp stop];
    return(thumbnail);
#endif
}

// After an Image has been picked up from Camera or Photo Library
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey:
                           UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(__bridge NSString *)kUTTypeImage]){
        mediaIndex = MEDIA_TYPE_PHOTO;
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        [self dismissViewControllerAnimated:NO completion:nil];
        _myImage.image = image;
        
        [self openImageEditor];
    }
    else if([mediaType isEqualToString:(__bridge NSString *)kUTTypeMovie]) {
        mediaIndex = MEDIA_TYPE_VIDEO;

        videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        UIImage *thumbImage = [self imageFromMovie:videoURL atTime:1];
        
        [self dismissViewControllerAnimated:NO completion:nil];
        _myImage.image = thumbImage;

        [self openImageEditor];
    }
    else
    {
        /* We failed to read the data. Use the dataReadingError
         variable to determine what the error is */
        NSLog(@"Failed to load the image");
    }

}


// Opens the image Editor screen
-(void)openImageEditor {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ImageEditor *imageEditor = (ImageEditor *)[storyboard instantiateViewControllerWithIdentifier:@"ImageEditor"];
    
    // Pass the captured Image
    imageEditor.imagePassed = _myImage.image;
    imageEditor.mediaIndex = mediaIndex;
    imageEditor.videoURL = videoURL;
    
    imageEditor.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:imageEditor animated:YES completion:nil];
}


-(void)initgAdBanner
{
    if (!self.gAdBannerView)
    {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )  {
            // iPad
            CGRect rect = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, GAD_SIZE_728x90.height);
            self.gAdBannerView = [[GADBannerView alloc] initWithFrame:rect];
        } else {
            // iPhone
            CGRect rect = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, GAD_SIZE_320x50.height);
            self.gAdBannerView = [[GADBannerView alloc] initWithFrame:rect];
        }
    }
    
    // ** IMPORTANT: YOU MUST COPY THE UNIT ID YOU'VE GOT FROM REGISTERING YOUR APP IN www.apps.admob.com HERE **
    self.gAdBannerView.adUnitID = @"ca-app-pub-1561400000155036/6597665700";
    
    self.gAdBannerView.rootViewController = self;
    self.gAdBannerView.delegate = self;
    self.gAdBannerView.hidden = YES;
    [self.view addSubview:self.gAdBannerView];
}

// Hide the banner by sliding down
-(void)hideBanner:(UIView*)banner
{
    if (banner && ![banner isHidden])
    {
        [UIView beginAnimations:@"hideBanner" context:nil];
        banner.frame = CGRectOffset(banner.frame, 0, banner.frame.size.height);
        [UIView commitAnimations];
        banner.hidden = TRUE;
    }
}

// Show the banner by sliding up
-(void)showBanner:(UIView*)banner
{
    if (banner && [banner isHidden])
    {
        [UIView beginAnimations:@"showBanner" context:nil];
        banner.frame = CGRectOffset(banner.frame, 0, -banner.frame.size.height);
        [UIView commitAnimations];
        banner.hidden = FALSE;
    }
}

// Called before ad is shown, good time to show the add
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    NSLog(@"Admob load");
    //[self hideBanner:self.iAdBannerView];
    [self showBanner:self.gAdBannerView];
}

// An error occured
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Admob error: %@", error);
    [self hideBanner:self.gAdBannerView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
