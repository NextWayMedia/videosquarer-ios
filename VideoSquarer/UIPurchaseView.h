//
//  UIPurchaseView.h
//  VideoSquarer
//
//  Created by Artem Volin on 3/18/15.
//  Copyright (c) 2015 FV iMAGINATION. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageEditor.h"

@interface UIPurchaseView : UIView
+ (id)instantation;

@property (nonatomic, assign)ImageEditor* controller;
@end
