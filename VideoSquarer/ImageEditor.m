//
//  ViewController.m
//  FilterX
//
//  Created by MacBook FV iMAGINATION on 17/07/14.
//  Copyright (c) 2014 FV iMAGINATION. All rights reserved.
//

#import "ImageEditor.h"
#import "MainScreen.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <GPUImage/GPUImage.h>
#import "Purchase.h"
#import "UIPurchaseView.h"

#define WATERMARK_MARGIN    40
#define WATERMARK_WIDTH     240
#define WATERMARK_HEIGHT    60
#define VIDEO_DEFAULT_WIDTH 640

#define BLUR_RADIUS_DEFAULT     10.f
#define BLUR_RADIUS_MAX         20.f
#define SHADOW_LENGTH_MAX       50.f
#define SHADOW_OFFSET           5.f

#define CONFIG_MARK_FIRST     @"MARK_FIRST"

@interface ImageEditor ()
{
    CIFilter *videoFilter;
    CGFloat mediaScale;
    CGPoint movePoint;
    BOOL isShareInstagram;
    UIView *    waitView;
    CGFloat imageExportWidth;
    CGFloat videoExportWidth;
    BOOL blurSelected;
    UIButton *borderButton;
    
    NSMutableArray *filtersArray;
    NSArray *filterTitleArray, *filterTitleArray2, *filterTitleArray3;
    int posX;
    UILabel *titleLabel2;
    
    int buttonTag;
    CGFloat blurRadius;
    CGRect shadowVideoRect;
    UIImage *shadowMaskImage;
    
    //UIImageView *filteredImageView;
    UIDocumentInteractionController *docInteraction;
    UIImage *combinedImage;
    
    // For Filter Packs
    int filterPackNr;
    int filterNr;
    
    // Gestures to move and scale the edited Image
    UIPanGestureRecognizer *panGest;
    UIPinchGestureRecognizer *pinchGest;
    BOOL moveScaleON;
    BOOL isShadow;
    
    UIColor *borderColor;
    NSInteger borderPatternIndex;
    int borderMode;
    
    GPUImageMovieWriter *movieWriter;
    GPUImageMovie *movieFile;
    GPUImageGaussianBlurFilter *videoBlurFilter;
    GPUImageCropFilter *videoCropFilter;
    NSTimer *timer;
    
    Purchase *purchase;
    CGAffineTransform userTransform;
    CGAffineTransform userVideoTransform;
    int rotateCount;
    BOOL isFirstRunning;
    BOOL isClickBlur;
    BOOL isWatermark;
    BOOL isAds;
    
}
-(void)shareToInstagram;
-(void)initiAdBanner;
-(void)initgAdBanner;
- (UIImage *)scaleAndRotateImage:(UIImage *)image;
@end

@implementation ImageEditor
@synthesize imagePassed;
@synthesize mediaIndex, videoURL, gInterstitialView, colorPickerView, bordersScrollView, blurCheckBtn;
@synthesize borderPattBtn, borderColorBtn, blurSlider;

-(BOOL)prefersStatusBarHidden {
    return true;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    purchase = [Purchase defaultPurchase];

    if(purchase.isRemoveWatermarkPurchased)
    {
        isWatermark = NO;
        _watermarkImageView.hidden = YES;
        _removeWatermarkButton.hidden = YES;
    }
    else
        isWatermark = YES;
    
    if(purchase.isRemoveAdsPurchased)
    {
        isAds = NO;
        _removeAdsButton.hidden = YES;
        _bottomBannerView.center = CGPointMake(_bottomBannerView.center.x, _bottomBannerView.center.y + 44);
        _bordersContainerView.center = CGPointMake(_bordersContainerView.center.x, _bordersContainerView.center.y + 44);
    }
    else
        isAds = YES;
    
    // Initializes the iAd & AdMob banners
    if(isAds)
    {
        [self initgAdBanner];
        [self.gAdBannerView loadRequest:[GADRequest request]];
        
        [self createAndLoadInterstitial];
    }
    [colorPickerView setDelegate:self];
    
    videoExportWidth = VIDEO_DEFAULT_WIDTH;
    blurSelected = NO;
    blurRadius = BLUR_RADIUS_DEFAULT;
    [blurSlider setMaximumValue:BLUR_RADIUS_MAX];
    isClickBlur = NO;
    
    //blurCheckBtn.adjustsImageWhenHighlighted = YES;
    
    waitView = [[UIView alloc] initWithFrame:self.view.bounds];
    waitView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    UIActivityIndicatorView * indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicatorView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [indicatorView setColor:[UIColor grayColor]];
    [indicatorView startAnimating];
    [indicatorView setCenter:self.view.center];
    
    [waitView addSubview:indicatorView];
    
    [waitView setBackgroundColor:[UIColor colorWithWhite:0.8 alpha:0.2]];
    [self.view addSubview:waitView];
    waitView.hidden = YES;
    
    /* Temporary Code */
    moveScaleON = YES;
    _filteredImagesScrollView.hidden = YES;
    isShareInstagram = NO;
    
    // Get the Image from Main Screen
    UIImage *drawImage = [self imageByRotatingImage:imagePassed fromImageOrientation:imagePassed.imageOrientation];
    
    //    CGFloat size = 0.f;
    //    CGFloat width = CGImageGetWidth(drawImage.CGImage), height = CGImageGetHeight(drawImage.CGImage);
    //    if(size < width)
    //        size = width;
    //    if(size < height)
    //        size = height;
    //    CGFloat frameWidth = _originalImage.frame.size.width;
    //
    //
    //    UIImage * image = [self UIImageScaleToSize:drawImage Size:CGSizeMake(width * frameWidth / size, height * frameWidth / size)];
    //    GPUImageHighlightShadowFilter *filter = [[GPUImageHighlightShadowFilter alloc] init];
    //    filter.shadows = 0.2;
    //drawImage = [filter imageByFilteringImage:drawImage];
    //    drawImage = [self imageApplyShadow:drawImage Shadow:1.f];
    //    drawImage = [drawImage imageWithShadowColor:[UIColor grayColor] offset:CGSizeMake(5.f, 5.f) blur:10.f];
    
    _filteredImageView.image = drawImage;
    
    filterPackNr = 1;
    mediaScale = 1.0f;
    movePoint = CGPointZero;
    
    // Resize the containerView and its images accordingly to the device used
    if ([[UIScreen mainScreen] bounds].size.width == 320) {
        //iphone 4 / 5
        if([[UIScreen mainScreen] bounds].size.height == 480) /* iphone 4 */
        {
            _containerView.frame = CGRectMake(20, 44, 280, 280);
            _filteredImagesScrollView.frame = CGRectMake(20, 44, 280, 280);
            
            _bottomBannerView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
            _bottomBannerView.frame = CGRectMake(0, 392, 320, 45);
            _bordersContainerView.frame = CGRectMake(320, 326, 320, 66);
        }
        else
        {
            _containerView.frame = CGRectMake(0, 44, 320, 320);
            _filteredImagesScrollView.frame = CGRectMake(0, 44, 320, 320);
            _bordersContainerView.frame = CGRectMake(_bordersContainerView.frame.size.width, _bottomBannerView.frame.origin.y - _bordersContainerView.frame.size.height,
                                                     _bordersContainerView.frame.size.width, _bordersContainerView.frame.size.height);

        }
    } else if ([[UIScreen mainScreen] bounds].size.width == 375) {
        // iphone 6
        _containerView.frame = CGRectMake(0, 44, 375, 375);
        _filteredImagesScrollView.frame = CGRectMake(0, 44, 375, 375);
        _bordersContainerView.frame = CGRectMake(_bordersContainerView.frame.size.width, _bottomBannerView.frame.origin.y - _bordersContainerView.frame.size.height,
                                                 _bordersContainerView.frame.size.width, _bordersContainerView.frame.size.height);
    } else if ([[UIScreen mainScreen] bounds].size.width == 414) {
        // iphone 6+
        _containerView.frame = CGRectMake(0, 44, 414, 414);
        _filteredImagesScrollView.frame = CGRectMake(0, 44, 414, 414);
        _bordersContainerView.frame = CGRectMake(_bordersContainerView.frame.size.width, _bottomBannerView.frame.origin.y - _bordersContainerView.frame.size.height,
                                                 _bordersContainerView.frame.size.width, _bordersContainerView.frame.size.height);
    }
    
    // Move the _bordersContainerView out of the screen
    //        _bordersContainerView.frame = CGRectMake(0, _containerView.frame.size.height+66, self.view.frame.size.width, 66);
    //        CGRect bordersFrame = _bordersContainerView.frame;
    //        bordersFrame.origin.x = self.view.frame.size.width;
    //        _bordersContainerView.frame = bordersFrame;
    //    CGRect rect = _bordersContainerView.frame;
    //    rect.origin.x = 0;
    //    rect.origin.y += 2;
    //    rect.size.height -= 4;
    //    _bordersContainerView.frame = rect;
    
    
    // Add PAN & PINCH Gesture Recogn. to the Image
    pinchGest = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(imageDidPinch:)];
    panGest = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(imageDidPan:)];
    [_containerView addGestureRecognizer:pinchGest];
    [_containerView addGestureRecognizer:panGest];
    
    
    // Round the buttons and Set Fonts =========
    _titleLabel.font = [UIFont fontWithName:@"What time is it?" size:18];
    _backOutlet.layer.cornerRadius = _backOutlet.bounds.size.width /2;
    _shareOutlet.layer.cornerRadius = _shareOutlet.bounds.size.width /2;
    
    // 1st Filter Pack Names Array ===============
    filterTitleArray = @[
                         @"A1",
                         @"A2",
                         @"A3",
                         @"A4",
                         ];
    // 2nd Filter Pack Names Array ===============
    filterTitleArray2 = @[
                          @"B1",
                          @"B2",
                          @"B3",
                          @"B4",
                          ];
    // 3nd Filter Pack Names Array ===============
    filterTitleArray3 = @[
                          @"C1",
                          @"C2",
                          @"C3",
                          @"C4",
                          ];
    
    
    
    // Call 1st Filters Pack (Default)
    [self initFilters1];
    
    // Set number of pages for PageControl
    _pageFilter.numberOfPages = filterTitleArray.count+1;
    
    // Set the color of the 1st FilterPack button (since it's active)
    [_filterPackOut1 setTitleColor:[UIColor colorWithRed:201.0/255.0 green:91.0/255.0 blue:96.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    
    borderMode = BACKGROUND_MODE_COLOR;
    borderPatternIndex = 0;
    borderColor = [UIColor whiteColor];
    _borderImage.image = [self imageWithColor:borderColor];

    // For Borders Buttons
    buttonTag = 0;
    [self setupBordersToolbar];
    userTransform = CGAffineTransformIdentity;
    userVideoTransform = CGAffineTransformIdentity;
    rotateCount = 0;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if(purchase.isBlurPurchased)
    {
        [userDefaults setBool:YES forKey:CONFIG_MARK_FIRST];
        [userDefaults synchronize];
    }
    else
    {
        NSNumber *val = [userDefaults objectForKey:CONFIG_MARK_FIRST];
        
        if(val && val.boolValue)
            isFirstRunning = NO;
        else
            isFirstRunning = YES;
        
        if(isFirstRunning)
        {
            val = [NSNumber numberWithBool:YES];
            [userDefaults setObject:val forKey:CONFIG_MARK_FIRST];
            [userDefaults synchronize];
            
            if(!purchase.isBlurPurchased)
            {
                UIPurchaseView *purchaseView = [UIPurchaseView instantation];
                CGRect rect = self.view.frame;
                rect.origin.y += self.view.frame.size.height;
                purchaseView.frame = rect;
                purchaseView.controller = self;
                [self.view addSubview:purchaseView];
                
                CGAffineTransform baseTransform = purchaseView.transform; //1
                baseTransform = CGAffineTransformTranslate(baseTransform,0, - rect.size.height); //2
                
                [UIView animateWithDuration: 0.5 animations:^{
                    purchaseView.transform = baseTransform; //3
                }];
            }
        }
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    /* adjust watermark position */
    CGFloat wmRatio = VIDEO_DEFAULT_WIDTH / _containerView.frame.size.width;
    CGFloat wmWidth = WATERMARK_WIDTH / wmRatio;
    CGFloat wmHeight = WATERMARK_HEIGHT / wmRatio;
    _watermarkImageView.frame = CGRectMake(_containerView.frame.size.width - wmWidth - WATERMARK_MARGIN / wmRatio,
                                           _containerView.frame.size.width - wmHeight - WATERMARK_MARGIN / wmRatio,
                                           wmWidth, wmHeight);
}

#pragma mark - FILTERS SCROLLVIEW METHODS ==============
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    CGFloat pageWidth = _filteredImagesScrollView.frame.size.width;
    filterNr = floor((_filteredImagesScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageFilter.currentPage = filterNr;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSLog(@"page:%ld", (long)_pageFilter.currentPage);
    
    if (filterPackNr == 1) {
        [self applyFilter1];
        
    } else if (filterPackNr == 2){
        [self applyFilter2];
        
    } else if (filterPackNr == 3) {
        [self applyFilter3];
    }
}


#pragma mark - INIT 1st FILTERS PACK ========================
-(void) initFilters1 {
    posX = 0;
    
    _filteredImagesScrollView.contentOffset = CGPointMake(0, 0);
    //    _filteredImageView.image = _originalImage.image;
    //    _originalImage.hidden = YES;
    
    for(UILabel *subview in [_filteredImagesScrollView subviews]) {
        [subview removeFromSuperview];
    }
    
    // LOOP ======
    for (int i = 0; i < filterTitleArray.count; i++) {
        posX = posX + self.view.frame.size.width;
        
        UILabel *titleLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 270, self.view.frame.size.width, 30)];
        titleLabel1.font = [UIFont fontWithName:@"What time is it?" size:18];
        titleLabel1.textColor = [UIColor whiteColor];
        titleLabel1.shadowColor = [UIColor darkGrayColor];
        titleLabel1.shadowOffset = CGSizeMake(1.0, 1.0);
        titleLabel1.textAlignment = NSTextAlignmentCenter;
        titleLabel1.text = @"Original";
        [_filteredImagesScrollView addSubview:titleLabel1];
        
        // Add a Label that shows the filter's name ===========
        titleLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(posX, 270, self.view.frame.size.width, 30)];
        titleLabel2.font = [UIFont fontWithName:@"What time is it?" size:22];
        titleLabel2.textColor = [UIColor whiteColor];
        titleLabel2.shadowColor = [UIColor darkGrayColor];
        titleLabel2.shadowOffset = CGSizeMake(1.0, 1.0);
        titleLabel2.textAlignment = NSTextAlignmentCenter;
        titleLabel2.text = [filterTitleArray objectAtIndex:i];
        [_filteredImagesScrollView addSubview:titleLabel2];
    }
    
    // Populate the ScrollView with all the filteres images
    _filteredImagesScrollView.contentSize = CGSizeMake(_filteredImagesScrollView.frame.size.width * (filterTitleArray.count +1), self.view.frame.size.width);
}

#pragma mark - APPLY FILTER1 METHOD ===============
-(void)applyFilter1 {
    // Load orignal image
    //    _filteredImageView.image = _originalImage.image;
    
    // Create filter
    UIImage *fixedImage = [self scaleAndRotateImage:_filteredImageView.image];
    CIImage *ciImage = [CIImage imageWithCGImage:fixedImage.CGImage];
    CIFilter *filter = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues:kCIInputImageKey, ciImage, nil];
    [filter setDefaults];
    
    /* Create Video Filter to compose video with filter when share to instagram. */
    videoFilter = [CIFilter filterWithName:@"CIColorMonochrome"];
    
    CGFloat r;
    CGFloat g;
    CGFloat b;
    CGFloat intensity;
    
    switch (filterNr) {
            // NO Filter
        case 0: {
            //            _filteredImageView.image = _originalImage.image;
            //            _originalImage.hidden = YES;
            break;  }
            
            // 1st FILTER =============
        case 1: {
            // PARAMETERS
            r = 255;
            g = 245;
            b = 250;
            intensity = 0.9;
            
            [filter setValue:[CIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0] forKey:@"inputColor"];
            [filter setValue:[NSNumber numberWithFloat:intensity] forKey:@"inputIntensity"];
            
            [videoFilter setValue:[CIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0] forKey:@"inputColor"];
            [videoFilter setValue:[NSNumber numberWithFloat:intensity] forKey:@"inputIntensity"];
            
            break; }
            
            
            // 2nd FILTER ================
        case 2: {
            // PARAMETERS
            r = 20;
            g = 0;
            b = 1;
            intensity = 0.9;
            
            [filter setValue:[CIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0] forKey:@"inputColor"];
            [filter setValue:[NSNumber numberWithFloat:intensity] forKey:@"inputIntensity"];
            
            [videoFilter setValue:[CIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0] forKey:@"inputColor"];
            [videoFilter setValue:[NSNumber numberWithFloat:intensity] forKey:@"inputIntensity"];
            
            break; }
            
            // 3rd FILTER =================
        case 3: {
            // PARAMETERS
            r = 150;
            g = 22;
            b = 80;
            intensity = 0.3;
            
            [filter setValue:[CIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0] forKey:@"inputColor"];
            [filter setValue:[NSNumber numberWithFloat:intensity] forKey:@"inputIntensity"];
            
            [videoFilter setValue:[CIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0] forKey:@"inputColor"];
            [videoFilter setValue:[NSNumber numberWithFloat:intensity] forKey:@"inputIntensity"];
            
            break; }
            
            
            // 4th FILTER =================
        case 4: {
            // PARAMETERS
            r = 240;
            g = 60;
            b = 159;
            intensity = 0.3;
            
            [filter setValue:[CIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0] forKey:@"inputColor"];
            [filter setValue:[NSNumber numberWithFloat:intensity] forKey:@"inputIntensity"];
            
            [videoFilter setValue:[CIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0] forKey:@"inputColor"];
            [videoFilter setValue:[NSNumber numberWithFloat:intensity] forKey:@"inputIntensity"];
            break; }
            
        default: break;
    }
    
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *filteredImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    if (filterNr >= 1) {
        _filteredImageView.image = filteredImage;
    }
}


#pragma mark - INIT 2nd FILTERS PACK ========================
-(void) initFilters2 {
    posX = 0;
    _filteredImagesScrollView.contentOffset = CGPointMake(0, 0);
    //    _filteredImageView.image = _originalImage.image;
    //    _originalImage.hidden = YES;
    
    for(UILabel *subview in [_filteredImagesScrollView subviews]) {
        [subview removeFromSuperview];
    }
    
    // LOOP ======
    for (int i = 0; i < filterTitleArray2.count; i++) {
        posX = posX + self.view.frame.size.width;
        
        UILabel *titleLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 270, self.view.frame.size.width, 30)];
        titleLabel1.font = [UIFont fontWithName:@"What time is it?" size:18];
        titleLabel1.textColor = [UIColor whiteColor];
        titleLabel1.shadowColor = [UIColor darkGrayColor];
        titleLabel1.shadowOffset = CGSizeMake(1.0, 1.0);
        titleLabel1.textAlignment = NSTextAlignmentCenter;
        titleLabel1.text = @"Original";
        [_filteredImagesScrollView addSubview:titleLabel1];
        
        // Add a Label that shows the filter's name ===========
        titleLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(posX, 270, self.view.frame.size.width, 30)];
        titleLabel2.font = [UIFont fontWithName:@"What time is it?" size:22];
        titleLabel2.textColor = [UIColor whiteColor];
        titleLabel2.shadowColor = [UIColor darkGrayColor];
        titleLabel2.shadowOffset = CGSizeMake(1.0, 1.0);
        titleLabel2.textAlignment = NSTextAlignmentCenter;
        titleLabel2.text = [filterTitleArray2 objectAtIndex:i];
        [_filteredImagesScrollView addSubview:titleLabel2];
    }
    
    // Populate the ScrollView with all the filteres images
    _filteredImagesScrollView.contentSize = CGSizeMake(_filteredImagesScrollView.frame.size.width * (filterTitleArray.count +1), self.view.frame.size.width);
}

#pragma mark - APPLY FILTER2 METHOD ===============
-(void)applyFilter2 {
    // Load orignal image
    //    _filteredImageView.image = _originalImage.image;
    
    // Create filter
    UIImage *fixedImage = [self scaleAndRotateImage:_filteredImageView.image];
    CIImage *ciImage = [CIImage imageWithCGImage:fixedImage.CGImage];
    CIFilter *filter;
    
    switch (filterNr) {
            // NO Filter
        case 0: {
            //            _filteredImageView.image = _originalImage.image;
            //            _originalImage.hidden = YES;
            break;  }
            
            
            // 1st FILTER =============
        case 1: {
            // PARAMETERS
            filter = [CIFilter filterWithName:@"CIPhotoEffectInstant" keysAndValues:kCIInputImageKey, ciImage, nil];
            /* Create Video Filter to compose video with filter when share to instagram. */
            videoFilter = [CIFilter filterWithName:@"CIPhotoEffectInstant"];
            
            [filter setDefaults];
            break; }
            
            
            // 2nd FILTER ================
        case 2: {
            // PARAMETERS
            filter = [CIFilter filterWithName:@"CIPhotoEffectProcess" keysAndValues:kCIInputImageKey, ciImage, nil];
            [filter setDefaults];
            
            /* Create Video Filter to compose video with filter when share to instagram. */
            videoFilter = [CIFilter filterWithName:@"CIPhotoEffectProcess"];
            
            break; }
            
            // 3rd FILTER =================
        case 3: {
            // PARAMETERS
            filter = [CIFilter filterWithName:@"CIPhotoEffectTransfer" keysAndValues:kCIInputImageKey, ciImage, nil];
            [filter setDefaults];
            
            /* Create Video Filter to compose video with filter when share to instagram. */
            videoFilter = [CIFilter filterWithName:@"CIPhotoEffectTransfer"];
            
            break; }
            
            
            // 4th FILTER =================
        case 4: {
            // PARAMETERS
            filter = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey, ciImage, nil];
            [filter setDefaults];
            
            /* Create Video Filter to compose video with filter when share to instagram. */
            videoFilter = [CIFilter filterWithName:@"CISepiaTone"];
            break; }
            
            
        default: break;
    }
    
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *filteredImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    if (filterNr >= 1) {
        _filteredImageView.image = filteredImage;
    }
    
}

#pragma mark - INIT 3rd FILTERS PACK ========================
-(void) initFilters3 {
    posX = 0;
    _filteredImagesScrollView.contentOffset = CGPointMake(0, 0);
    //    _filteredImageView.image = _originalImage.image;
    //    _originalImage.hidden = YES;
    
    for(UILabel *subview in [_filteredImagesScrollView subviews]) {
        [subview removeFromSuperview];
    }
    
    // LOOP ======
    for (int i = 0; i < filterTitleArray3.count; i++) {
        posX = posX + self.view.frame.size.width;
        
        UILabel *titleLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 270, self.view.frame.size.width, 30)];
        titleLabel1.font = [UIFont fontWithName:@"What time is it?" size:18];
        titleLabel1.textColor = [UIColor whiteColor];
        titleLabel1.shadowColor = [UIColor darkGrayColor];
        titleLabel1.shadowOffset = CGSizeMake(1.0, 1.0);
        titleLabel1.textAlignment = NSTextAlignmentCenter;
        titleLabel1.text = @"Original";
        [_filteredImagesScrollView addSubview:titleLabel1];
        
        // Add a Label that shows the filter's name ===========
        titleLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(posX, 270, self.view.frame.size.width, 30)];
        titleLabel2.font = [UIFont fontWithName:@"What time is it?" size:22];
        titleLabel2.textColor = [UIColor whiteColor];
        titleLabel2.shadowColor = [UIColor darkGrayColor];
        titleLabel2.shadowOffset = CGSizeMake(1.0, 1.0);
        titleLabel2.textAlignment = NSTextAlignmentCenter;
        titleLabel2.text = [filterTitleArray3 objectAtIndex:i];
        [_filteredImagesScrollView addSubview:titleLabel2];
    }
    
    // Populate the ScrollView with all the filteres images
    _filteredImagesScrollView.contentSize = CGSizeMake(_filteredImagesScrollView.frame.size.width * (filterTitleArray3. count +1), self.view.frame.size.width);
}

#pragma mark - APPLY FILTER3 METHOD ===============
-(void)applyFilter3 {
    // Load orignal image
    //    _filteredImageView.image = _originalImage.image;
    
    // Create filter
    UIImage *fixedImage = [self scaleAndRotateImage:_filteredImageView.image];
    CIImage *ciImage = [CIImage imageWithCGImage:fixedImage.CGImage];
    CIFilter *filter;
    
    
    switch (filterNr) {
            // NO Filter
        case 0: {
            //            _filteredImageView.image = _originalImage.image;
            //            _originalImage.hidden = YES;
            break;  }
            
            
            // 1st FILTER =============
        case 1: {
            // PARAMETERS
            filter = [CIFilter filterWithName:@"CIPhotoEffectFade" keysAndValues:kCIInputImageKey, ciImage, nil];
            [filter setDefaults];
            
            /* Create Video Filter to compose video with filter when share to instagram. */
            videoFilter = [CIFilter filterWithName:@"CIPhotoEffectFade"];
            
            break; }
            
            
            // 2nd FILTER ================
        case 2: {
            // PARAMETERS
            filter = [CIFilter filterWithName:@"CIPhotoEffectTonal" keysAndValues:kCIInputImageKey, ciImage, nil];
            [filter setDefaults];
            
            /* Create Video Filter to compose video with filter when share to instagram. */
            videoFilter = [CIFilter filterWithName:@"CIPhotoEffectTonal"];
            
            break; }
            
            // 3rd FILTER =================
        case 3: {
            // PARAMETERS
            filter = [CIFilter filterWithName:@"CIPhotoEffectNoir" keysAndValues:kCIInputImageKey, ciImage, nil];
            [filter setDefaults];
            
            /* Create Video Filter to compose video with filter when share to instagram. */
            videoFilter = [CIFilter filterWithName:@"CIPhotoEffectNoir"];
            
            break; }
            
            
            // 4th FILTER =================
        case 4: {
            // PARAMETERS
            filter = [CIFilter filterWithName:@"CIDotScreen" keysAndValues:kCIInputImageKey, ciImage, nil];
            [filter setDefaults];
            CIVector *vct = [[CIVector alloc] initWithX:_filteredImageView.image.size.width/2
                                                      Y:_filteredImageView.image.size.height/2];
            [filter setValue:vct forKey:@"inputCenter"];
            [filter setValue:[NSNumber numberWithFloat:5.00] forKey:@"inputWidth"];
            [filter setValue:[NSNumber numberWithFloat:5.00] forKey:@"inputAngle"];
            [filter setValue:[NSNumber numberWithFloat:0.70] forKey:@"inputSharpness"];
            
            /* Create Video Filter to compose video with filter when share to instagram. */
            videoFilter = [CIFilter filterWithName:@"CIDotScreen"];
            [videoFilter setValue:vct forKey:@"inputCenter"];
            [videoFilter setValue:[NSNumber numberWithFloat:5.00] forKey:@"inputWidth"];
            [videoFilter setValue:[NSNumber numberWithFloat:5.00] forKey:@"inputAngle"];
            [videoFilter setValue:[NSNumber numberWithFloat:0.70] forKey:@"inputSharpness"];
            
            break; }
            
        default: break;
    }
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *filteredImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    if (filterNr >= 1) {
        _filteredImageView.image = filteredImage;
    }
}


- (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - SETUP BORDERS TOOLBAR =====================
-(void)setupBordersToolbar {
    
    int buttonsQty = 7;
    int xCoord = 0;
    int yCoord = 0;
    int buttonWidth = bordersScrollView.frame.size.height;
    int buttonHeight = bordersScrollView.frame.size.height;
    int gapBetweenButtons = 0;
    
    // Loop for creating buttons
    for (int i = 1; i <= buttonsQty; i++)
    {
        buttonTag++;
        NSString *imageStr = [NSString stringWithFormat:@"%d", i];
        
        borderButton = [UIButton buttonWithType:UIButtonTypeCustom];
        borderButton.frame = CGRectMake(xCoord, yCoord, buttonWidth,buttonHeight);
        borderButton.tag = buttonTag;
        [borderButton setBackgroundImage:[UIImage imageNamed:imageStr] forState:UIControlStateNormal];
        [borderButton addTarget:self action:@selector(bordersButtTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [bordersScrollView addSubview:borderButton];
        
        xCoord += buttonWidth + gapBetweenButtons;
    }
    
    bordersScrollView.contentSize = CGSizeMake(buttonWidth * (buttonsQty), yCoord);
    [self.view bringSubviewToFront:_bordersContainerView];
    
}
-(void)bordersButtTapped: (UIButton *)button {
    borderMode = BACKGROUND_MODE_PATTERN;
    borderPatternIndex = button.tag;
    NSString *borderStr = [NSString stringWithFormat:@"%ld.png", (long)button.tag];
    _borderImage.image = [UIImage imageNamed:borderStr];
    NSLog(@"%@", borderStr);
}

#pragma mark - ColorPickerView Delegate =====================
//Action when user is choosing color
-(void)colorIsChanging:(UIColor *)color{
    borderMode = BACKGROUND_MODE_COLOR;
    borderColor = color;
    _borderImage.image = [self imageWithColor:color];;
}

//Action when user finished to select a color
-(void)colorSelected:(UIColor *)color{
    borderMode = BACKGROUND_MODE_COLOR;
    borderColor = color;
    
    _borderImage.image = [self imageWithColor:color];;
}

#pragma mark - GESTURE RECOGNIZERS =============
-(void)imageDidPinch: (UIPinchGestureRecognizer *) sender {
    if(!moveScaleON)
        return;
    
    mediaScale *= sender.scale;
    NSLog(@"scale = %f", mediaScale);
    _filteredImageView.transform = CGAffineTransformScale(_filteredImageView.transform, sender.scale, sender.scale);
    sender.scale = 1;
}

- (void)imageDidPan:(UIPanGestureRecognizer*)recognizer
{
    if(!moveScaleON)
        return;
    
    CGPoint translation = [recognizer translationInView:self.view];
    CGPoint orgTranslation =[recognizer translationInView:self.view];
    
    movePoint.x += orgTranslation.x;
    movePoint.y += orgTranslation.y;
    
    NSLog(@"movePoint x = %f, y = %f", movePoint.x, movePoint.y);
    
    _filteredImageView.center = CGPointMake(_filteredImageView.center.x + translation.x, _filteredImageView.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
}

#pragma mark - APPLY BLUR EFFECT ===============

- (UIImage*)UIImageScaleToSize:(UIImage*)image Size:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, size.width, size.height), image.CGImage);
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

- (UIImage*) processBlurEffectToImage:(UIImage*)image
{
    CGImageRef cgImage = image.CGImage;
    CGFloat width = CGImageGetWidth(cgImage), height = CGImageGetHeight(cgImage);
    CGFloat small_size = 0;
    if(width > height)
    {
        small_size = height * 8 / 10;
    }
    else if(height > width)
    {
        small_size = width * 8 / 10;
    }
    else
    {
        small_size = width * 8 / 10;
    }
    
    CGRect cropRect = CGRectMake((width - small_size) / 2, (height - small_size) / 2, small_size, small_size);
    // Crop logic
    CGImageRef imageRef = CGImageCreateWithImageInRect(cgImage, cropRect);
    UIImage * croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    UIImage *ret = [self imageApplyBlur:croppedImage withRadius:blurRadius];
    return ret;
}

- (UIImage *)imageApplyBlur: (UIImage *)imageToBlur
                 withRadius: (CGFloat)val {
    
    GPUImageGaussianBlurFilter *blurFilter = [[GPUImageGaussianBlurFilter alloc] init];
    blurFilter.blurRadiusInPixels = val;
    return [blurFilter imageByFilteringImage:imageToBlur];
}

#pragma mark - APPLY SHADOW EFFECT ===============

- (UIImage *)getShadowMaskImage
{
    UIImage *drawImage = _filteredImageView.image;
    CGImageRef cgImage;
    if(rotateCount)
    {
        CIImage* coreImage = drawImage.CIImage;
        if (!coreImage) {
            coreImage = [CIImage imageWithCGImage:drawImage.CGImage];
        }
        
        coreImage = [coreImage imageByApplyingTransform:userTransform];
        drawImage = [UIImage imageWithCIImage:coreImage];
        CIContext *ciContext = [CIContext contextWithOptions:nil];
        cgImage = [ciContext createCGImage:drawImage.CIImage fromRect:drawImage.CIImage.extent];
    }
    else
        cgImage = drawImage.CGImage;
    
    CGFloat width = drawImage.size.width;
    CGFloat height = drawImage.size.height;
    CGFloat squareSize;
    if(width > height)
        squareSize = width;
    else
        squareSize = height;
    
    squareSize = ((int)squareSize >> 2) << 2;
    
    CGFloat offset = SHADOW_OFFSET * squareSize / _containerView.frame.size.width;
    CALayer *mainLayer = [CALayer layer];
    mainLayer.frame = CGRectMake(0, 0, width + offset * 4, height + offset * 4);
    
    CALayer *shadowLayer = [CALayer layer];
    shadowLayer.frame = CGRectMake(offset * 2, offset * 2, width, height);
    shadowVideoRect = mainLayer.frame;
    shadowLayer.contents = (__bridge id) cgImage;
    
    shadowLayer.cornerRadius = offset;
    shadowLayer.shadowColor = [[UIColor blackColor] CGColor];
    shadowLayer.shadowOpacity = 1;
    shadowLayer.shadowRadius = offset;
    shadowLayer.shadowOffset = CGSizeMake(0, 0);
    
    [mainLayer addSublayer:shadowLayer];
    UIGraphicsBeginImageContext(mainLayer.frame.size);
    [mainLayer renderInContext:UIGraphicsGetCurrentContext()];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClearRect(context, shadowLayer.frame);
    
    drawImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if(rotateCount)
        CGImageRelease(cgImage);
    return drawImage;
}

- (IBAction)touchShadowButton:(id)sender {
    isShadow = !isShadow;
    
    if (isShadow) {
        [_shadowBtn setBackgroundImage:[UIImage imageNamed:@"shadow_button_on"] forState:UIControlStateNormal];
        
        CGRect rect = _filteredImageView.frame;
        rect.origin.x += SHADOW_OFFSET * 2 * mediaScale;
        rect.origin.y += SHADOW_OFFSET * 2 * mediaScale;
        
        rect.size.width -= SHADOW_OFFSET * 4 * mediaScale;
        rect.size.height -= SHADOW_OFFSET * 4 * mediaScale;
        
        _filteredImageView.frame = rect;
        _filteredImageView.layer.cornerRadius = SHADOW_OFFSET;
        _filteredImageView.layer.shadowColor = [[UIColor blackColor] CGColor];
        _filteredImageView.layer.shadowOpacity = 1;
        _filteredImageView.layer.shadowRadius = SHADOW_OFFSET;
        _filteredImageView.layer.shadowOffset = CGSizeMake(0, 0);
        
    } else {
        [_shadowBtn setBackgroundImage:[UIImage imageNamed:@"shadow_button_off"] forState:UIControlStateNormal];
        //_filteredImageView.image = _originalImage.image;
        
        CGRect rect = _filteredImageView.frame;
        rect.origin.x -= SHADOW_OFFSET * 2 * mediaScale;
        rect.origin.y -= SHADOW_OFFSET * 2 * mediaScale;
        
        rect.size.width += SHADOW_OFFSET * 4 * mediaScale;
        rect.size.height += SHADOW_OFFSET * 4 * mediaScale;
        
        _filteredImageView.frame = rect;
        
        _filteredImageView.layer.shadowOpacity = 0;
    }
}

#pragma mark - BUTTONS ======================
- (IBAction)filterPackButt1:(id)sender {
    filterPackNr = 1;
    [self initFilters1];
    NSLog(@"fPack:%d", filterPackNr);
    [_filterPackOut1 setTitleColor:[UIColor colorWithRed:201.0/255.0 green:91.0/255.0 blue:96.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_filterPackOut2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_filterPackOut3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)filterPackButt2:(id)sender {
    filterPackNr = 2;
    [self initFilters2];
    NSLog(@"fPack:%d", filterPackNr);
    [_filterPackOut1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_filterPackOut2 setTitleColor:[UIColor colorWithRed:201.0/255.0 green:91.0/255.0 blue:96.0/255.0 alpha:1.0] forState:UIControlStateNormal];    [_filterPackOut3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
}
- (IBAction)filterPackButt3:(id)sender {
    filterPackNr = 3;
    [self initFilters3];
    NSLog(@"fPack:%d", filterPackNr);
    [_filterPackOut1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_filterPackOut2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_filterPackOut3 setTitleColor:[UIColor colorWithRed:201.0/255.0 green:91.0/255.0 blue:96.0/255.0 alpha:1.0] forState:UIControlStateNormal];
}
- (IBAction)blurValueChanged:(id)sender {
    int val = (int)blurSlider.value;
    
    if((int)blurRadius != val)
    {
        blurRadius = (CGFloat)val;
        [self applyBlurToUI];
    }
}

- (void)applyBlurToUI
{
    UIImage *image = [self processBlurEffectToImage:_filteredImageView.image];
    if(rotateCount)
    {
        CIImage* coreImage = image.CIImage;
        if (!coreImage) {
            coreImage = [CIImage imageWithCGImage:image.CGImage];
        }
        
        coreImage = [coreImage imageByApplyingTransform:userTransform];
        image = [UIImage imageWithCIImage:coreImage];
        
    }
    _borderImage.image = image;
}

- (void)goBlurEffect
{
    blurSelected = !blurSelected;
    [blurCheckBtn setSelected:blurSelected];
    if(blurSelected)
    {
        bordersScrollView.hidden = YES;
        colorPickerView.hidden = YES;
        blurSlider.hidden = NO;
        [blurSlider setValue:blurRadius];
        [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
            CGRect rect = _bordersContainerView.frame;
            rect.origin.x = 0;
//            rect.origin.y += 1;
//            rect.size.height -= 2;
            
            _bordersContainerView.frame = rect;
        } completion:^(BOOL finished) {
        }];
    }
    
    waitView.hidden = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if(blurSelected)
        {
            [borderColorBtn setEnabled:NO];
            [borderPattBtn setEnabled:NO];
            blurSlider.enabled = YES;

            [self applyBlurToUI];
        }
        else
        {
            [borderColorBtn setEnabled:YES];
            [borderPattBtn setEnabled:YES];
            blurSlider.enabled = NO;
            
            if(borderMode == BACKGROUND_MODE_COLOR)
            {
                _borderImage.image = [self imageWithColor:borderColor];
            }
            else if(borderMode == BACKGROUND_MODE_PATTERN)
            {
                NSString *borderStr = [NSString stringWithFormat:@"%ld.png", (long)borderPatternIndex];
                _borderImage.image = [UIImage imageNamed:borderStr];
            }
        }
        
        waitView.hidden = YES;
    });
}

-(void) purchaseBlur
{
    waitView.hidden = NO;
    [purchase buyBlurCompletionHandler:^(BOOL isSuccess) {
        dispatch_async(dispatch_get_main_queue(), ^{
            waitView.hidden = YES;
        });
        
        if(isSuccess && isClickBlur)
        {
            [self goBlurEffect];
            isClickBlur = NO;
        }
    }];
}

- (IBAction)blurCheckButt:(id)sender {
    if(purchase.isBlurPurchased)
    {
        [self goBlurEffect];
    }
    else
    {
        isClickBlur = YES;
        
        UIPurchaseView *purchaseView = [UIPurchaseView instantation];
        CGRect rect = self.view.frame;
        rect.origin.y += self.view.frame.size.height;
        purchaseView.frame = rect;
        purchaseView.controller = self;
        [self.view addSubview:purchaseView];
        
        CGAffineTransform baseTransform = purchaseView.transform; //1
        baseTransform = CGAffineTransformTranslate(baseTransform,0, - rect.size.height); //2
        
        [UIView animateWithDuration: 0.5 animations:^{
            purchaseView.transform = baseTransform; //3
        }];
        
    }
}

- (IBAction)borderColorButt:(id)sender {
    bordersScrollView.hidden = YES;
    blurSlider.hidden = YES;
    colorPickerView.hidden = NO;
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
        CGRect rect = _bordersContainerView.frame;
        rect.origin.x = 0;
        //rect.origin.y += 1;
        //rect.size.height -= 2;
        
        _bordersContainerView.frame = rect;
    } completion:^(BOOL finished) {
    }];
    
}

- (IBAction)bordersButt:(id)sender {
    // Show Borders Buttons menu
    bordersScrollView.hidden = NO;
    colorPickerView.hidden = YES;
    blurSlider.hidden = YES;
    
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
        CGRect rect = _bordersContainerView.frame;
        rect.origin.x = 0;
//        rect.origin.y += 1;
//        rect.size.height -= 2;
        
        _bordersContainerView.frame = rect;
    } completion:^(BOOL finished) {
    }];
}

- (IBAction)closeBordersButt:(id)sender {
    // Hide Borders Buttons menu
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
        CGRect rect = _bordersContainerView.frame;
        rect.origin.x = self.view.frame.size.width;
//        rect.origin.y -= 1;
//        rect.size.height += 2;
        
        _bordersContainerView.frame = rect;
    } completion:^(BOOL finished) {
    }];
}


- (IBAction)moveScaleButt:(id)sender {
    moveScaleON = !moveScaleON;
    NSLog(@"moveScale:%d", moveScaleON);
    
    if (moveScaleON) {
        //        _filteredImagesScrollView.hidden = YES;
        [_moveScaleOutlet setBackgroundImage:[UIImage imageNamed:@"moveScaleButtON"] forState:UIControlStateNormal];
    } else {
        //        _filteredImagesScrollView.hidden = NO;
        [_moveScaleOutlet setBackgroundImage:[UIImage imageNamed:@"moveScaleButt"] forState:UIControlStateNormal];
    }
}

- (IBAction)saveToLibraryButt:(id)sender {
    isShareInstagram = NO;
    
    if (gInterstitialView.isReady) {
        [gInterstitialView presentFromRootViewController:self];
    } else {
        [self processShareToSave];
    }
}

-(void)processShareToSave
{
    waitView.hidden = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if(mediaIndex == MEDIA_TYPE_PHOTO)
        {
            [self processResultImage];
            waitView.hidden = YES;
            
            NSString *messageStr  = @"Check this cool picture, I've made it with #VideoSquarer!";
            NSArray *shareItems = @[combinedImage , messageStr];
            
            UIActivityViewController* activityViewController =
            [[UIActivityViewController alloc] initWithActivityItems: shareItems
                                              applicationActivities:nil];
            activityViewController.excludedActivityTypes = @[UIActivityTypePrint,
                                                             UIActivityTypeCopyToPasteboard];
            
            [self presentViewController:activityViewController animated:YES completion:nil];
        }
        else
            [self processResultVideo];
        
    });
}

-(UIImage*)imageByRotatingImage:(UIImage*)initImage fromImageOrientation:(UIImageOrientation)orientation
{
    CGImageRef imgRef = initImage.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = orientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            return initImage;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    // Create the bitmap context
    CGContextRef    context = NULL;
    void *          bitmapData;
    int             bitmapByteCount;
    int             bitmapBytesPerRow;
    
    // Declare the number of bytes per row. Each pixel in the bitmap in this
    // example is represented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    bitmapBytesPerRow   = (bounds.size.width * 4);
    bitmapByteCount     = (bitmapBytesPerRow * bounds.size.height);
    bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        return nil;
    }
    
    // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
    // per component. Regardless of what the source image format is
    // (CMYK, Grayscale, and so on) it will be converted over to the format
    // specified here by CGBitmapContextCreate.
    CGColorSpaceRef colorspace = CGImageGetColorSpace(imgRef);
    context = CGBitmapContextCreate (bitmapData,bounds.size.width,bounds.size.height,8,bitmapBytesPerRow,
                                     colorspace, kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);
    
    if (context == NULL)
        // error creating context
        return nil;
    
    CGContextScaleCTM(context, -1.0, -1.0);
    CGContextTranslateCTM(context, -bounds.size.width, -bounds.size.height);
    
    CGContextConcatCTM(context, transform);
    
    // Draw the image to the bitmap context. Once we draw, the memory
    // allocated for the context for rendering will then contain the
    // raw image data in the specified color space.
    CGContextDrawImage(context, CGRectMake(0,0,width, height), imgRef);
    
    CGImageRef imgRef2 = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    free(bitmapData);
    UIImage * image = [UIImage imageWithCGImage:imgRef2 scale:initImage.scale orientation:UIImageOrientationUp];
    CGImageRelease(imgRef2);
    return image;
}

- (void)processResultImage
{
#if 0
    // Crop a Combined Image from the edited picture
    CGRect rect = [_containerView bounds];
    UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
    [_containerView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    combinedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
#else
    CGFloat width = CGImageGetWidth(imagePassed.CGImage);
    CGFloat height = CGImageGetHeight(imagePassed.CGImage);
    
    if(width > height)
        imageExportWidth = width;
    else
        imageExportWidth = height;
    
    imageExportWidth = ((int)imageExportWidth >> 2) << 2;
    //UIImage *drawImage = [self imageByRotatingImage:imagePassed fromImageOrientation:imagePassed.imageOrientation];
    UIImage *drawImage = _filteredImageView.image;
    UIImage *frontImage = drawImage;
    
    if(rotateCount)
    {
        CIImage* coreImage = frontImage.CIImage;
        if (!coreImage) {
            coreImage = [CIImage imageWithCGImage:frontImage.CGImage];
        }
        
        coreImage = [coreImage imageByApplyingTransform:userTransform];

        CIContext *ciContext = [CIContext contextWithOptions:nil];
        CGImageRef cgImage = [ciContext createCGImage:coreImage fromRect:coreImage.extent];

        frontImage = [UIImage imageWithCGImage:cgImage];
        coreImage = nil;
    }
    
    if(isShadow)
    {
        CGFloat offset = SHADOW_OFFSET * imageExportWidth / _containerView.frame.size.width;
        CALayer *mainLayer = [CALayer layer];
        mainLayer.frame = CGRectMake(0, 0, CGImageGetWidth(frontImage.CGImage) + offset * 4, CGImageGetHeight(frontImage.CGImage) + offset * 4);
        
        CALayer *shadowLayer = [CALayer layer];
        shadowLayer.frame = CGRectMake(offset * 2, offset * 2, CGImageGetWidth(frontImage.CGImage), CGImageGetHeight(frontImage.CGImage));
        shadowLayer.contents = (id) frontImage.CGImage;
        
        shadowLayer.cornerRadius = offset;
        shadowLayer.shadowColor = [[UIColor blackColor] CGColor];
        shadowLayer.shadowOpacity = 1;
        shadowLayer.shadowRadius = offset;
        shadowLayer.shadowOffset = CGSizeMake(0, 0);
        
        [mainLayer addSublayer:shadowLayer];
        UIGraphicsBeginImageContext(mainLayer.frame.size);
        [mainLayer renderInContext:UIGraphicsGetCurrentContext()];
        
        frontImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        width = CGImageGetWidth(frontImage.CGImage) + offset * 4;
        height = CGImageGetHeight(frontImage.CGImage) + offset * 4;
    }
    UIGraphicsBeginImageContext(CGSizeMake(imageExportWidth, imageExportWidth));
    
    //    if(blurSelected)
    //    {
    //        UIImage *image = [self processBlurEffectToImage:drawImage];
    //        [image drawInRect:CGRectMake(0, 0, imageExportWidth, imageExportWidth)];
    //    }
    //    else
    [_borderImage.image drawInRect:CGRectMake(0, 0, imageExportWidth, imageExportWidth)];
    
    //    drawImage = imagePassed.CGImage;
    //    if(videoFilter)
    //    {
    //        [videoFilter setDefaults];
    //        [videoFilter setValue:_borderImage.image.CIImage forKey:kCIInputImageKey];
    //
    //        CIContext *context = [CIContext contextWithOptions:nil];
    //        CIImage *outputImage = [videoFilter outputImage];
    //        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    //        drawImage = cgImage;
    //    }
    //
    //    UIImage *img = [UIImage imageWithCGImage:drawImage];
    //    UIImageOrientation orientation = imagePassed.imageOrientation;
    //
    //    BOOL isPortrait;
    //    if(orientation == UIImageOrientationRight || orientation == UIImageOrientationLeft)
    //        isPortrait = YES;
    //    else
    //        isPortrait = NO;
    
    width = frontImage.size.width;
    height = frontImage.size.height;
    
    CGFloat widthRatio;
    if(width > height)
        widthRatio = imageExportWidth / width;
    else
        widthRatio = imageExportWidth / height;
    
    CGPoint videoPoint;
    videoPoint.x = movePoint.x * imageExportWidth / _borderImage.frame.size.width;
    videoPoint.y = movePoint.y * imageExportWidth / _borderImage.frame.size.height;
    
    //    if(isPortrait){
    //        if(width > height)
    //        {
    //            [frontImage drawInRect:CGRectMake((imageExportWidth - widthRatio * height * mediaScale) / 2.0f + videoPoint.x,
    //                                             (imageExportWidth - imageExportWidth * mediaScale) / 2.0f + videoPoint.y,
    //                                             height * widthRatio * mediaScale, width * widthRatio * mediaScale)];
    //        }
    //        else
    //        {
    //            [frontImage drawInRect:CGRectMake((imageExportWidth - imageExportWidth * mediaScale) / 2.0f + videoPoint.x,
    //                                             (imageExportWidth - widthRatio * width * mediaScale) / 2 + videoPoint.y,
    //                                             height * widthRatio * mediaScale, width * widthRatio * mediaScale)];
    //        }
    //    }
    //    else{
    if(width > height)
    {
        [frontImage drawInRect:CGRectMake((imageExportWidth - imageExportWidth * mediaScale) / 2.0f + videoPoint.x,
                                          (imageExportWidth - widthRatio * height * mediaScale) / 2 + videoPoint.y,
                                          width * widthRatio * mediaScale, height * widthRatio * mediaScale)];
    }
    else
    {
        [frontImage drawInRect:CGRectMake((imageExportWidth - widthRatio * width * mediaScale) / 2.0f + videoPoint.x,
                                          (imageExportWidth - imageExportWidth * mediaScale) / 2.0f + videoPoint.y,
                                          width * widthRatio * mediaScale, height * widthRatio * mediaScale)];
    }
    //    }
    
    if(isWatermark)
    {
        UIImage *wmImage = [UIImage imageNamed:@"VideoSquarerWatermark.png"];
        
        CGFloat wmRatio = VIDEO_DEFAULT_WIDTH / imageExportWidth;
        [wmImage drawInRect:CGRectMake((VIDEO_DEFAULT_WIDTH - WATERMARK_WIDTH - WATERMARK_MARGIN) / wmRatio,
                                       (VIDEO_DEFAULT_WIDTH - WATERMARK_HEIGHT - WATERMARK_MARGIN) / wmRatio,
                                       WATERMARK_WIDTH / wmRatio, WATERMARK_HEIGHT / wmRatio)];
    }
    //    if(videoFilter)
    //        CGImageRelease(drawImage);
    
    combinedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
#endif
}

- (void)processBlurVideoWithCompletionHandler:(void (^)(void))handler
{
    /* Convert blurred video. */
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
    //                                                         NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //    NSString *blurFilePath = [NSString stringWithFormat:@"%@.mp4", [documentsDirectory stringByAppendingPathComponent:@"vqtemp_blur"]];
    
    NSString *blurFilePath = [NSString stringWithFormat:@"%@.mp4", [NSTemporaryDirectory() stringByAppendingPathComponent:@"vqtemp_blur"]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:blurFilePath])
        [fileManager removeItemAtPath:blurFilePath error:nil];
    
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:videoURL options:nil];
    AVAssetTrack *videoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize videoSize = [videoTrack naturalSize];
    CGAffineTransform transform = videoTrack.preferredTransform;
    
    videoAsset = nil;
    
    movieFile = [[GPUImageMovie alloc] initWithURL:videoURL];
    movieFile.runBenchmark = NO;
    movieFile.playAtActualSpeed = NO;
    
    CGFloat small_size = 0;
    CGFloat width = videoSize.width, height = videoSize.height;
    if(width > height)
    {
        small_size = height * 8 / 10;
    }
    else if(height > width)
    {
        small_size = width * 8 / 10;
    }
    else
    {
        small_size = width * 8 / 10;
    }
    
    CGRect cropRect = CGRectMake(((width - small_size) / 2) / width, ((height - small_size) / 2) / height, small_size / width, small_size / height);
    
    videoCropFilter = [[GPUImageCropFilter alloc] init];
    [videoCropFilter setCropRegion:cropRect];
    
    videoBlurFilter = [[GPUImageGaussianBlurFilter alloc] init];
    videoBlurFilter.blurRadiusInPixels = blurRadius;
    
    [movieFile addTarget:videoCropFilter];
    [videoCropFilter addTarget:videoBlurFilter];
    
    // Only rotate the video for display, leave orientation the same for recording
    //        GPUImageView *filterView = (GPUImageView *)self.view;
    //        [filter addTarget:filterView];
    
    // In addition to displaying to the screen, write out a processed version of the movie to disk
    
    movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:[NSURL fileURLWithPath:blurFilePath] size:CGSizeMake(VIDEO_DEFAULT_WIDTH, VIDEO_DEFAULT_WIDTH)];
    [videoBlurFilter addTarget:movieWriter];
    
    // Configure this for video from the movie file, where we want to preserve all video frames and audio samples
    movieWriter.shouldPassthroughAudio = YES;
    movieFile.audioEncodingTarget = nil;
    
    [movieFile enableSynchronizedEncodingUsingMovieWriter:movieWriter];
    
    [movieWriter startRecordingInOrientation:transform];
    //[movieWriter startRecording];
    [movieFile startProcessing];
    
    //    timer = [NSTimer scheduledTimerWithTimeInterval:0.3f
    //                                             target:self
    //                                           selector:@selector(retrievingProgress)
    //                                           userInfo:nil
    //                                            repeats:YES];
    
    [movieWriter setCompletionBlock:^{
        [videoBlurFilter removeTarget:movieWriter];
        [movieWriter finishRecording];
        
        //        if(isShadow)
        //        {
        //            [self processShadowVideoWithCompletionHandler:^{
        //                handler();
        //            }];
        //        }
        //        else
        handler();
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [timer invalidate];
        });
    }];
}

- (void)retrievingProgress
{
    NSLog(@"%@", [NSString stringWithFormat:@"%d%%", (int)(movieFile.progress * 100)]);
}

- (void)processResultVideo
{
    if(isShadow)
        shadowMaskImage = [self getShadowMaskImage];
    
    if(blurSelected)
    {
        [self processBlurVideoWithCompletionHandler:^{
            [self processVideoLast];
        }];
    }
    else
        [self processVideoLast];
}

- (void) processShadowVideoWithCompletionHandler:(void (^)(void))handler
{
    NSString *tmpFilePath = [NSString stringWithFormat:@"%@.mp4", [NSTemporaryDirectory() stringByAppendingPathComponent:@"vq_shadow"]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:tmpFilePath])
        [fileManager removeItemAtPath:tmpFilePath error:nil];
    
    NSString *blurFilePath = [NSString stringWithFormat:@"%@.mp4", [NSTemporaryDirectory() stringByAppendingPathComponent:@"vqtemp_blur"]];
    
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:[NSURL fileURLWithPath:blurFilePath] options:nil];
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    
    AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo  preferredTrackID:kCMPersistentTrackID_Invalid];
    AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                                   ofTrack:clipVideoTrack
                                    atTime:kCMTimeZero error:nil];
    
    [compositionVideoTrack setPreferredTransform:[[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] preferredTransform]];
    
    AVMutableVideoCompositionLayerInstruction* layerInstruction1 = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:compositionVideoTrack];
    
    CGSize videoSize = [[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] naturalSize];
    NSLog(@"Video size(%f, %f)", videoSize.width, videoSize.height);
    
    /* Create Video Composition to insert watermark layer to video */
    AVMutableVideoComposition* videoComp = [AVMutableVideoComposition videoComposition];
    videoComp.renderSize = videoSize;
    videoComp.frameDuration = CMTimeMake(1, 30);
    
    /* create video and sub layer */
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, videoSize.width, videoSize.height);
    videoLayer.frame = parentLayer.frame;
    
    CALayer *shadowLayer = [CALayer layer];
    shadowLayer.contents = (id)imagePassed.CGImage;
    CGFloat width = CGImageGetWidth(imagePassed.CGImage);
    CGFloat height = CGImageGetHeight(imagePassed.CGImage);
    
    CGFloat offset = SHADOW_OFFSET * VIDEO_DEFAULT_WIDTH / _containerView.frame.size.width;
    
    CGRect rect;
    
    UIImageOrientation orientation = imagePassed.imageOrientation;
    
    BOOL isPortrait;
    if(orientation == UIImageOrientationRight || orientation == UIImageOrientationLeft)
        isPortrait = YES;
    else
        isPortrait = NO;
    
    CGFloat widthRatio;
    if(width > height)
        widthRatio = VIDEO_DEFAULT_WIDTH / width;
    else
        widthRatio = VIDEO_DEFAULT_WIDTH / height;
    
    CGPoint videoPoint;
    videoPoint.x = movePoint.x * VIDEO_DEFAULT_WIDTH / _borderImage.frame.size.width;
    videoPoint.y = movePoint.y * VIDEO_DEFAULT_WIDTH / _borderImage.frame.size.height;
    
    if(isPortrait){
        if(width > height)
        {
            rect = CGRectMake((VIDEO_DEFAULT_WIDTH - widthRatio * height * mediaScale) / 2.0f + videoPoint.x,
                              (VIDEO_DEFAULT_WIDTH - VIDEO_DEFAULT_WIDTH * mediaScale) / 2.0f - videoPoint.y,
                              height * widthRatio * mediaScale, width * widthRatio * mediaScale);
        }
        else
        {
            rect = CGRectMake((VIDEO_DEFAULT_WIDTH - VIDEO_DEFAULT_WIDTH * mediaScale) / 2.0f + videoPoint.x,
                              (VIDEO_DEFAULT_WIDTH - widthRatio * width * mediaScale) / 2 - videoPoint.y,
                              height * widthRatio * mediaScale, width * widthRatio * mediaScale);
        }
    }else{
        if(width > height)
        {
            rect = CGRectMake((VIDEO_DEFAULT_WIDTH - VIDEO_DEFAULT_WIDTH * mediaScale) / 2.0f + videoPoint.x,
                              (VIDEO_DEFAULT_WIDTH - widthRatio * height * mediaScale) / 2 - videoPoint.y,
                              width * widthRatio * mediaScale, height * widthRatio * mediaScale);
        }
        else
        {
            rect = CGRectMake((VIDEO_DEFAULT_WIDTH - widthRatio * width * mediaScale) / 2.0f + videoPoint.x,
                              (VIDEO_DEFAULT_WIDTH - VIDEO_DEFAULT_WIDTH * mediaScale) / 2.0f - videoPoint.y,
                              width * widthRatio * mediaScale, height * widthRatio * mediaScale);
        }
    }
    
    CGFloat widthMargin, heightMargin;
    
    widthMargin = ((1.f - (_containerView.frame.size.width - SHADOW_OFFSET * 4) / _containerView.frame.size.width) * rect.size.width ) / 2;
    heightMargin = ((1.f - (_containerView.frame.size.width - SHADOW_OFFSET * 4) / _containerView.frame.size.width) * rect.size.height ) / 2;
    rect.origin.x += widthMargin;
    rect.origin.y += widthMargin;
    
    rect.size.width *= (_containerView.frame.size.width - SHADOW_OFFSET * 4) / _containerView.frame.size.width;
    rect.size.height *= (_containerView.frame.size.width - SHADOW_OFFSET * 4) / _containerView.frame.size.width;
    
    shadowLayer.frame = rect;
    shadowLayer.cornerRadius = offset;
    shadowLayer.shadowColor = [[UIColor blackColor] CGColor];
    shadowLayer.shadowOpacity = 1;
    shadowLayer.shadowRadius = offset;
    shadowLayer.shadowOffset = CGSizeMake(0, 0);
    
    
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:shadowLayer];
    
    videoComp.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    
    AVMutableVideoCompositionInstruction *MainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mixComposition duration]);
    NSLog(@"%lld", mixComposition.duration.value );
    MainInstruction.layerInstructions = [NSArray arrayWithObject:layerInstruction1];
    
    videoComp.instructions = [NSArray arrayWithObject: MainInstruction];
    
    AVAssetExportSession *assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];//AVAssetExportPresetPassthrough
    assetExport.videoComposition = videoComp;
    
    NSURL *exportUrl = [NSURL fileURLWithPath:tmpFilePath];
    
    assetExport.outputFileType = AVFileTypeQuickTimeMovie;
    assetExport.outputURL = exportUrl;
    assetExport.shouldOptimizeForNetworkUse = YES;
    
    [assetExport exportAsynchronouslyWithCompletionHandler:
     ^(void ) {
         
         switch (assetExport.status)
         {
             case AVAssetExportSessionStatusUnknown:
                 NSLog(@"Unknown");
                 break;
             case AVAssetExportSessionStatusWaiting:
                 NSLog(@"Waiting");
                 break;
             case AVAssetExportSessionStatusExporting:
                 NSLog(@"Exporting");
                 break;
             case AVAssetExportSessionStatusCompleted:
                 handler();
                 break;
             case AVAssetExportSessionStatusFailed:
                 waitView.hidden = YES;
             {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                 message:@"Failed to export the video!"
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 [alert show];
             }
                 break;
             case AVAssetExportSessionStatusCancelled:
                 waitView.hidden = YES;
                 
             {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                 message:@"Cancelled to export the video!"
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 [alert show];
             }
                 break;
         }
         
         //UISaveVideoAtPathToSavedPhotosAlbum(tmpFilePath, self, @selector(videoSavedCallback:saveResultError:ctx:), nil);
     }
     ];
    
}
- (void) processVideoLast
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *tmpFilePath = [NSString stringWithFormat:@"%@.mp4", [NSTemporaryDirectory() stringByAppendingPathComponent:@"vqtemp"]];
    
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
    //                                                         NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //    NSString *blurFilePath = [NSString stringWithFormat:@"%@.mp4", [documentsDirectory stringByAppendingPathComponent:@"vqtemp_blur"]];
    
    NSString *blurFilePath = nil;
    
    blurFilePath = [NSString stringWithFormat:@"%@.mp4", [NSTemporaryDirectory() stringByAppendingPathComponent:@"vqtemp_blur"]];
    
    
    if([fileManager fileExistsAtPath:tmpFilePath])
        [fileManager removeItemAtPath:tmpFilePath error:nil];
    
    NSURL *orgURL = nil;
    //    if(isShadow)
    //        orgURL =  [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@.mp4", [NSTemporaryDirectory() stringByAppendingPathComponent:@"vq_shadow"]]];
    //    else
    orgURL = videoURL;
    
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:orgURL options:nil];
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    
    AVURLAsset *blurAsset = nil;
    AVMutableCompositionTrack *backgroundCompositionTrack = nil;
    AVAssetTrack *blurVideoTrack = nil;
    
    AVMutableCompositionTrack *compositionAudioTrack = nil;
    
    AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo  preferredTrackID:kCMPersistentTrackID_Invalid];
    AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVAssetTrack *clipAudioTrack = [[videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    
    CGSize videoSize = [[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] naturalSize];
    NSLog(@"Video size(%f, %f)", videoSize.width, videoSize.height);
    
    CGAffineTransform firstTransform = clipVideoTrack.preferredTransform;
    BOOL  isFirstAssetPortrait_  = NO;
    UIImageOrientation FirstAssetOrientation_  = UIImageOrientationUp;
    
    if(firstTransform.a == 0 && firstTransform.b == 1.0 && firstTransform.c == -1.0 && firstTransform.d == 0)  {FirstAssetOrientation_= UIImageOrientationRight; isFirstAssetPortrait_ = YES;}
    if(firstTransform.a == 0 && firstTransform.b == -1.0 && firstTransform.c == 1.0 && firstTransform.d == 0)  {FirstAssetOrientation_ =  UIImageOrientationLeft; isFirstAssetPortrait_ = YES;}
    if(firstTransform.a == 1.0 && firstTransform.b == 0 && firstTransform.c == 0  && firstTransform.d == 1.0)   {FirstAssetOrientation_ =  UIImageOrientationUp;}
    if(firstTransform.a == -1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == -1.0) {FirstAssetOrientation_ = UIImageOrientationDown;}
    
    if(rotateCount)
    {
        CGAffineTransform a;
        CGFloat offsetX, offsetY;
        
        if(isFirstAssetPortrait_)
        {
            offsetX = videoSize.width;
            offsetY = videoSize.height;
        }
        else
        {
            offsetX = videoSize.height;
            offsetY = videoSize.width;
        }
        switch (rotateCount) {
            case 1: /* 90' */
                a = CGAffineTransformConcat(userVideoTransform, CGAffineTransformMakeTranslation(offsetX, 0));
                
                break;
            case 2: /* 180' */
                a = CGAffineTransformConcat(userVideoTransform, CGAffineTransformMakeTranslation(offsetY, offsetX));
                break;
            case 3: /* 270' */
                a = CGAffineTransformMakeRotation(- M_PI_2);
                a = CGAffineTransformConcat(a, CGAffineTransformMakeTranslation(0, offsetY));
                break;
            default:
                break;
        }
        firstTransform = CGAffineTransformConcat(firstTransform, a);
    }
    
    if(clipAudioTrack != nil)
    {
        compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                                       ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
    }
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                                   ofTrack:clipVideoTrack
                                    atTime:kCMTimeZero error:nil];
    
    [compositionVideoTrack setPreferredTransform:clipVideoTrack.preferredTransform];
    
    if(blurSelected)
    {
        blurAsset = [[AVURLAsset alloc]initWithURL:[NSURL fileURLWithPath:blurFilePath] options:nil];
        backgroundCompositionTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        
        blurVideoTrack = [[blurAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        
        [backgroundCompositionTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                                            ofTrack:blurVideoTrack atTime:kCMTimeZero error:nil];
        [backgroundCompositionTrack setPreferredTransform:[blurVideoTrack preferredTransform]];
    }
    
    
    CGFloat VideoWidth = videoExportWidth;
    
    /* Create Video Composition to insert watermark layer to video */
    AVMutableVideoComposition* videoComp = [AVMutableVideoComposition videoComposition];
    videoComp.renderSize = CGSizeMake(VideoWidth, VideoWidth);
    videoComp.frameDuration = CMTimeMake(1, 30);
    
    CGFloat widthRatio;
    if(videoSize.width > videoSize.height)
        widthRatio = VideoWidth / videoSize.width;
    else
        widthRatio = VideoWidth / videoSize.height;
    
    AVMutableVideoCompositionLayerInstruction* layerInstruction1 = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:compositionVideoTrack];
    
    BOOL frontPortrait_  = NO;
    FirstAssetOrientation_  = UIImageOrientationUp;
    
    if(firstTransform.b == 1.0 && firstTransform.c == -1.0)  {FirstAssetOrientation_= UIImageOrientationRight; frontPortrait_ = YES;}
    if(firstTransform.b == -1.0 && firstTransform.c == 1.0)  {FirstAssetOrientation_ =  UIImageOrientationLeft; frontPortrait_ = YES;}
    //    if(firstTransform.a == 1.0 && firstTransform.b == 0 && firstTransform.c == 0  && firstTransform.d == 1.0)   {FirstAssetOrientation_ =  UIImageOrientationUp;}
    //    if(firstTransform.a == -1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == -1.0) {FirstAssetOrientation_ = UIImageOrientationDown;}
    
    CGAffineTransform FirstAssetScaleFactor;
    if(frontPortrait_){
        if(videoSize.width > videoSize.height)
            FirstAssetScaleFactor = CGAffineTransformMakeScale(VideoWidth / videoSize.height, widthRatio);
        else
            FirstAssetScaleFactor = CGAffineTransformMakeScale(widthRatio, VideoWidth / videoSize.width);

        [layerInstruction1 setTransform:CGAffineTransformConcat(CGAffineTransformConcat(firstTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(0, 0)) atTime:kCMTimeZero];
    }else{
        if(videoSize.width > videoSize.height)
            FirstAssetScaleFactor = CGAffineTransformMakeScale(widthRatio, VideoWidth / videoSize.height);
        else
            FirstAssetScaleFactor = CGAffineTransformMakeScale(VideoWidth / videoSize.width, widthRatio);

        [layerInstruction1 setTransform:CGAffineTransformConcat(CGAffineTransformConcat(firstTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(0, 0)) atTime:kCMTimeZero];
    }
    
    CGPoint videoPoint;
    videoPoint.x = movePoint.x * VideoWidth / _borderImage.frame.size.width;
    videoPoint.y = movePoint.y * VideoWidth / _borderImage.frame.size.width;

    CGFloat scaleVal = mediaScale;
    CGFloat margin = 0.f;
    if(isShadow)
    {
        //CGFloat offset = SHADOW_OFFSET * VideoWidth / _originalImage.frame.size.width;
        CGFloat widthMargin, heightMargin;
        
        widthMargin = ((1.f - (_containerView.frame.size.width - SHADOW_OFFSET * 4) / _containerView.frame.size.width) * videoSize.width ) / 2;
        heightMargin = ((1.f - (_containerView.frame.size.width - SHADOW_OFFSET * 4) / _containerView.frame.size.width) * videoSize.height ) / 2;
        
        scaleVal = mediaScale * (_containerView.frame.size.width - SHADOW_OFFSET * 4) / _containerView.frame.size.width;
        //videoPoint.x += widthMargin;
        //videoPoint.y -= heightMargin / 2;
        
    }
    FirstAssetScaleFactor = CGAffineTransformMakeScale(widthRatio * scaleVal, widthRatio * scaleVal);
    CGFloat videoRatio = widthRatio * scaleVal;
    CGAffineTransform videoTransform;
    if(frontPortrait_){
        
        if(videoSize.width > videoSize.height)
        {
            videoTransform = CGAffineTransformConcat(CGAffineTransformConcat(firstTransform, FirstAssetScaleFactor),
                                                     CGAffineTransformMakeTranslation((VideoWidth - videoRatio * videoSize.height) / 2 + videoPoint.x,
                                                                                      (VideoWidth - videoSize.width * videoRatio) / 2 + videoPoint.y));
        }
        else
        {
            videoTransform = CGAffineTransformConcat(CGAffineTransformConcat(firstTransform, FirstAssetScaleFactor),
                                                     CGAffineTransformMakeTranslation((VideoWidth - videoSize.height * videoRatio) / 2 + videoPoint.x,
                                                                                      (VideoWidth - videoRatio * videoSize.width) / 2 + videoPoint.y));
        }
    }else{
        if(videoSize.width > videoSize.height)
        {
            videoTransform = CGAffineTransformConcat(CGAffineTransformConcat(firstTransform, FirstAssetScaleFactor),
                                                     CGAffineTransformMakeTranslation((VideoWidth - videoSize.width * videoRatio) / 2 + videoPoint.x,
                                                                                      (VideoWidth - videoRatio * videoSize.height) / 2 + videoPoint.y));
        }
        else
        {
            videoTransform = CGAffineTransformConcat(CGAffineTransformConcat(firstTransform, FirstAssetScaleFactor),
                                                     CGAffineTransformMakeTranslation((VideoWidth - videoRatio * videoSize.width) / 2 + videoPoint.x,
                                                                                      (VideoWidth - videoSize.height * videoRatio) / 2 + videoPoint.y));
        }
    }

    /* Create Border layer */
    
    CALayer *aLayer = [CALayer layer];
    aLayer.contents = (id)_borderImage.image.CGImage;
    aLayer.frame = CGRectMake(0, 0, VideoWidth, VideoWidth);
    aLayer.opacity = 1.0f; //Feel free to alter the alpha here
    
    /* create video and sub layer */
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    
    parentLayer.frame = CGRectMake(0, 0, VideoWidth, VideoWidth);
    
    if(blurSelected)
    {
        videoLayer.frame = parentLayer.frame;
    }
    else
    {
        CGRect videoRect = CGRectApplyAffineTransform(CGRectMake(0.f, 0.f, videoSize.width, videoSize.height), videoTransform);
        /* inverse y coordinate */
        videoRect.origin.y = VIDEO_DEFAULT_WIDTH - videoRect.size.height - videoRect.origin.y;
        videoLayer.frame = videoRect;
        
        if(isShadow)
        {
            CGFloat offset = SHADOW_OFFSET * VideoWidth / _containerView.frame.size.width;
            
            videoLayer.cornerRadius = offset;
            videoLayer.shadowColor = [[UIColor blackColor] CGColor];
            videoLayer.shadowOpacity = 1;
            videoLayer.shadowRadius = offset;
            videoLayer.shadowOffset = CGSizeMake(0, 0);
        }
    }
    
    [parentLayer addSublayer:aLayer];
    [parentLayer addSublayer:videoLayer];

    if(isWatermark)
    {
        UIImage *wmImage = [UIImage imageNamed:@"VideoSquarerWatermark.png"];
        
        CALayer *wmLayer = [CALayer layer];
        wmLayer.contents = (id)wmImage.CGImage;
        wmLayer.frame = CGRectMake(VideoWidth - WATERMARK_WIDTH - WATERMARK_MARGIN, WATERMARK_MARGIN, WATERMARK_WIDTH, WATERMARK_HEIGHT);
        wmLayer.opacity = 1.0f; //Feel free to alter the alpha here
        
        [parentLayer addSublayer:wmLayer];
    }
    
    if(blurSelected)
    {
        videoComp.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    }
    else
        videoComp.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    
    AVMutableVideoCompositionInstruction *MainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mixComposition duration]);
    NSLog(@"%lld", mixComposition.duration.value );
    
    if(blurSelected)
    {
        AVMutableVideoCompositionLayerInstruction *backgroundInstrunction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:backgroundCompositionTrack];
        
        CGAffineTransform scale = CGAffineTransformMakeScale(1.f,1.f);
        CGAffineTransform move = CGAffineTransformMakeTranslation(0,0);
        
        
        CGAffineTransform secondTransform = clipVideoTrack.preferredTransform;
        if(rotateCount)
        {
            CGAffineTransform a;
            if(isFirstAssetPortrait_)
            {
                switch (rotateCount) {
                    case 1: /* 90' */
                        a = CGAffineTransformConcat(userVideoTransform, CGAffineTransformMakeTranslation(VIDEO_DEFAULT_WIDTH, - (videoSize.height - VIDEO_DEFAULT_WIDTH)));
                        
                        break;
                    case 2: /* 180' */
                        a = CGAffineTransformConcat(userVideoTransform, CGAffineTransformMakeTranslation(VIDEO_DEFAULT_WIDTH + (videoSize.height - VIDEO_DEFAULT_WIDTH), VIDEO_DEFAULT_WIDTH));
                        break;
                    case 3: /* 270' */
                        a = CGAffineTransformMakeRotation(- M_PI_2);
                        a = CGAffineTransformConcat(a, CGAffineTransformMakeTranslation(0, videoSize.height));
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (rotateCount) {
                    case 1: /* 90' */
                        a = CGAffineTransformConcat(userVideoTransform, CGAffineTransformMakeTranslation(VIDEO_DEFAULT_WIDTH, 0));
                        
                        break;
                    case 2: /* 180' */
                        a = CGAffineTransformConcat(userVideoTransform, CGAffineTransformMakeTranslation(VIDEO_DEFAULT_WIDTH, VIDEO_DEFAULT_WIDTH));
                        break;
                    case 3: /* 270' */
                        a = CGAffineTransformMakeRotation(- M_PI_2);
                        a = CGAffineTransformConcat(a, CGAffineTransformMakeTranslation(0, VIDEO_DEFAULT_WIDTH));
                        break;
                    default:
                        break;
                }
                
            }
            secondTransform = CGAffineTransformConcat(secondTransform, a);
        }
        else
        {
            if(frontPortrait_)
            {
                if(videoSize.width > videoSize.height)
                    move = CGAffineTransformMakeTranslation(VideoWidth - videoSize.height, 0);
                else
                    move = CGAffineTransformMakeTranslation(VideoWidth - videoSize.width, 0);
            }
        }
        
        [backgroundInstrunction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(secondTransform, scale), move) atTime:kCMTimeZero];
        
        //        CGAffineTransform Scale = CGAffineTransformMakeScale(0.7f,0.7f);
        //        CGAffineTransform Move = CGAffineTransformMakeTranslation(200, 200);
        
        [layerInstruction1 setTransform:videoTransform atTime:kCMTimeZero];
        
        if(isShadow)
        {
            CGRect videoRect = CGRectApplyAffineTransform(CGRectMake(0.f, 0.f, videoSize.width, videoSize.height), videoTransform);
            
            CGRect maskRect = CGRectMake(0, 0, shadowVideoRect.size.width * widthRatio * scaleVal, shadowVideoRect.size.height * widthRatio * scaleVal);
            CGFloat offsetX, offsetY;
            offsetX = (maskRect.size.width - videoRect.size.width) / 2;
            offsetY = (maskRect.size.height - videoRect.size.height) / 2;
            
            CGRect drawMaskRect = CGRectMake(videoRect.origin.x - offsetX, VIDEO_DEFAULT_WIDTH - (videoRect.origin.y + offsetY +videoRect.size.height),
                                             maskRect.size.width, maskRect.size.height);
            CALayer *shadowLayer = [CALayer layer];
            shadowLayer.contents = (id)shadowMaskImage.CGImage;
            shadowLayer.frame = drawMaskRect;
            
            [videoLayer addSublayer:shadowLayer];
        }
        
        [layerInstruction1 setOpacity:1.0f atTime:kCMTimeZero];
        MainInstruction.layerInstructions = [NSArray arrayWithObjects:layerInstruction1, backgroundInstrunction, nil];
    }
    else
        MainInstruction.layerInstructions = [NSArray arrayWithObject:layerInstruction1];
    
    videoComp.instructions = [NSArray arrayWithObject: MainInstruction];
    
    AVAssetExportSession *assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];//AVAssetExportPresetPassthrough
    assetExport.videoComposition = videoComp;
    
    NSURL *exportUrl = [NSURL fileURLWithPath:tmpFilePath];
    
    assetExport.outputFileType = AVFileTypeQuickTimeMovie;
    assetExport.outputURL = exportUrl;
    assetExport.shouldOptimizeForNetworkUse = YES;
    
    [assetExport exportAsynchronouslyWithCompletionHandler:
     ^(void ) {
         dispatch_async(dispatch_get_main_queue(), ^{
             waitView.hidden = YES;
         });
         
         switch (assetExport.status)
         {
             case AVAssetExportSessionStatusUnknown:
                 NSLog(@"Unknown");
                 break;
             case AVAssetExportSessionStatusWaiting:
                 NSLog(@"Waiting");
                 break;
             case AVAssetExportSessionStatusExporting:
                 NSLog(@"Exporting");
                 break;
             case AVAssetExportSessionStatusCompleted:
                 if(isShareInstagram)
                 {
                     ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                     [library writeVideoAtPathToSavedPhotosAlbum:exportUrl completionBlock:^(NSURL *assetURL, NSError *error) {
                         NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@", assetURL.absoluteString]];
                         
                         NSURL *testURL = [NSURL URLWithString:@"instagram://camera"];
                         if ([[UIApplication sharedApplication] canOpenURL:testURL])
                         {
                             [[UIApplication sharedApplication] openURL:instagramURL];
                         }
                         else
                         {
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                             message:@"Instagram is not installed on your device."
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil];
                             [alert show];
                         }
                         
                     }];
                 }
                 
                 else
                 {
                     NSString *messageStr  = @"Check this video, I've made it with #VideoSquarer!";
                     NSArray *shareItems = @[messageStr, exportUrl];
                     
                     UIActivityViewController* activityViewController =
                     [[UIActivityViewController alloc] initWithActivityItems: shareItems
                                                       applicationActivities:nil];
                     activityViewController.excludedActivityTypes = @[UIActivityTypePrint,
                                                                      UIActivityTypeCopyToPasteboard];
                     
                     [self presentViewController:activityViewController animated:YES completion:nil];
                 }
                 break;
             case AVAssetExportSessionStatusFailed:
             {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                 message:@"Failed to export the video!"
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 [alert show];
             }
                 break;
             case AVAssetExportSessionStatusCancelled:
             {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                 message:@"Cancelled to export the video!"
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 [alert show];
             }
                 break;
         }
         
         //UISaveVideoAtPathToSavedPhotosAlbum(tmpFilePath, self, @selector(videoSavedCallback:saveResultError:ctx:), nil);
     }
     ];
}

- (IBAction)shareButt:(id)sender {
    isShareInstagram = YES;
    if (gInterstitialView.isReady) {
        [gInterstitialView presentFromRootViewController:self];
    } else {
        [self processShareToInstagram];
    }
    
}

- (void)processShareToInstagram
{
    waitView.hidden = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if(mediaIndex == MEDIA_TYPE_PHOTO)
        {
            [self processResultImage];
            waitView.hidden = YES;
            // Call the DocumentInteractionController for Instagram Sharing
            [self shareToInstagram];
            
        }
        else
        {
            [self processResultVideo];
        }
    });
    
}


-(void)videoSavedCallback:(NSString *)videoPath saveResultError:(NSError *)error ctx:(void *)ctx
{
    if(error==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Successful!"
                              message:@"Video Save to camera roll."
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Error!"
                              message:[NSString stringWithFormat:@"%@",error]
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    //[[NSFileManager defaultManager] removeItemAtPath:videoPath error:nil];
}

- (IBAction)backButt:(id)sender {
    UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"VideoSquarer"
                                                     message:@"Are you sure you want to exit?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"YES", nil];
    myAlert.tag = 1000;
    myAlert.delegate = self;
    [myAlert show];
}


- (IBAction)onRotateButton:(id)sender {
    _filteredImageView.transform = CGAffineTransformRotate(_filteredImageView.transform, M_PI_2);
    userTransform = CGAffineTransformRotate(userTransform, -M_PI_2);
    userVideoTransform = CGAffineTransformRotate(userVideoTransform, M_PI_2);
    rotateCount = ++rotateCount % 4;
    if(blurSelected)
    {
        UIImage *image = _borderImage.image;
        CIImage* coreImage = image.CIImage;
        if (!coreImage) {
            coreImage = [CIImage imageWithCGImage:image.CGImage];
        }
        
        coreImage = [coreImage imageByApplyingTransform:CGAffineTransformMakeRotation(-M_PI_2)];
        _borderImage.image = [UIImage imageWithCIImage:coreImage];
    }
}


- (IBAction)onRemoveAds:(id)sender {
    UIAlertView *alertRemoveWatermark = [[UIAlertView alloc] initWithTitle:@"Remove Ads" message:@"If you wish to remove Ads, click the Remove button below. One time payment of $1.99" delegate:self cancelButtonTitle:@"No Thanks" otherButtonTitles:@"Remove - $1.99",nil];
    alertRemoveWatermark.tag = 1002;
    alertRemoveWatermark.delegate = self;
    [alertRemoveWatermark show];
}

- (IBAction)onRemoveWatermark:(id)sender {
    UIAlertView *alertRemoveWatermark = [[UIAlertView alloc] initWithTitle:@"Remove Watermark" message:@"If you wish to save photos & videos without watermark, click the Remove button below. One time payment of $1.99" delegate:self cancelButtonTitle:@"No Thanks" otherButtonTitles:@"Remove - $1.99",nil];
    alertRemoveWatermark.tag = 1001;
    alertRemoveWatermark.delegate = self;
    [alertRemoveWatermark show];
}

#pragma mark - INSTAGRAM SHARING METHOD =====================
-(void)shareToInstagram {
    /* =================
     NOTE: The following methods work only on real device, not iOS Simulator, and you should have Instagram already installed into your device!
     ================= */
    
    CGRect rect = CGRectMake(0 ,0 , 0, 0);
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        
        docInteraction.delegate = self;
        
        //Saves the edited Image to directory
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"instaFilterx.jpg"];
        UIImage *image = combinedImage;
        NSData *imageData = UIImagePNGRepresentation(image);
        [imageData writeToFile:savedImagePath atomically:NO];
        
        //Loads the edited Image
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"instaFilterx.jpg"];
        UIImage *tempImage = [UIImage imageWithContentsOfFile:getImagePath];
        
        //Hooks the edited Image with Instagram
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/instaFilterx.igo"];
        [UIImageJPEGRepresentation(tempImage, 1.0) writeToFile:jpgPath atomically:YES];
        
        // Prepares the DocumentInteraction with the .igo image for Instagram
        NSURL *instagramImageURL = [[NSURL alloc] initFileURLWithPath:jpgPath];
        docInteraction = [UIDocumentInteractionController interactionControllerWithURL:instagramImageURL];
        [docInteraction setUTI:@"com.instagram.exclusivegram"];
        
        // Opens the DocumentInteraction Menu
        [docInteraction presentOpenInMenuFromRect:rect inView:self.view animated:YES];
        
        
        //Disable iCloud Backup for Image URL
        NSError *error = nil;
        BOOL success = [instagramImageURL setResourceValue: [NSNumber numberWithBool: true] forKey: NSURLIsExcludedFromBackupKey error: &error];
        if(!success){
            NSLog(@"Error excluding %@ from backup %@", [instagramImageURL lastPathComponent], error);
        }else{
            NSLog(@"Success excluding %@ from backup %@", [instagramImageURL lastPathComponent], error);
        }
        
        
    } else {
        // Opens an AlertView as sharing result when the Document Interaction Controller gets dismissed
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Instagram is not installed on your device."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        NSLog(@"No Instagram Found");
    }
}

#pragma mark - UIAlertViewDelegate METHODS ==================

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case 9876:
            if (buttonIndex==0)
            {
                [purchase buyBlurCompletionHandler:^(BOOL isSuccess) {
                    waitView.hidden = YES;
                    if(isSuccess)
                        [self goBlurEffect];
                }];
            }
            else
            {
                [alertView dismissWithClickedButtonIndex:1 animated:NO];
            }
            break;
            
        case 1000:
            if (buttonIndex==1)
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                MainScreen *mainVC = (MainScreen *)[storyboard instantiateViewControllerWithIdentifier:@"MainScreen"];
                mainVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                
                filtersArray = nil;
                imagePassed = nil;
                
                [self presentViewController:mainVC animated:YES completion:nil];
            }
            break;
        case 1001:
            if (buttonIndex==1)
            {
                waitView.hidden = NO;
                [purchase buyRemoveWatermarkCompletionHandler:^(BOOL isSuccess) {
                    waitView.hidden = YES;
                    if(isSuccess)
                    {
                        _watermarkImageView.hidden = YES;
                        isWatermark = NO;
                        _removeWatermarkButton.hidden = YES;
                    }
                }];

            }
            break;
        case 1002:
            if (buttonIndex==1)
            {
                waitView.hidden = NO;
                [purchase buyRemoveAdsCompletionHandler:^(BOOL isSuccess) {
                    waitView.hidden = YES;
                    if(isSuccess)
                    {
                        isAds = NO;
                        _removeAdsButton.hidden = YES;
                        [_gAdBannerView removeFromSuperview];
                        _bottomBannerView.center = CGPointMake(_bottomBannerView.center.x, _bottomBannerView.center.y + 44);
                        _bordersContainerView.center = CGPointMake(_bordersContainerView.center.x, _bordersContainerView.center.y + 44);
                    }
                }];
            }
            break;
        default:
            break;
    }
}

#pragma mark - iAd + AdMob BANNER METHODS ==================

-(void)initiAdBanner
{
    if (!self.iAdBannerView)
    {
        CGRect rect = CGRectMake(0, self.view.frame.size.height, 0, 0);
        self.iAdBannerView = [[ADBannerView alloc]initWithFrame:rect];
        self.iAdBannerView.delegate = self;
        self.iAdBannerView.hidden = TRUE;
        [self.view addSubview:self.iAdBannerView];
    }
}

-(void)initgAdBanner
{
    if (!self.gAdBannerView)
    {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )  {
            // iPad
            CGRect rect = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, GAD_SIZE_728x90.height);
            self.gAdBannerView = [[GADBannerView alloc] initWithFrame:rect];
        } else {
            // iPhone
            CGRect rect = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, GAD_SIZE_320x50.height);
            self.gAdBannerView = [[GADBannerView alloc] initWithFrame:rect];
        }
    }
    
    // ** IMPORTANT: YOU MUST COPY THE UNIT ID YOU'VE GOT FROM REGISTERING YOUR APP IN www.apps.admob.com HERE **
    self.gAdBannerView.adUnitID = @"ca-app-pub-1561400000155036/6597665700";
    
    self.gAdBannerView.rootViewController = self;
    self.gAdBannerView.delegate = self;
    self.gAdBannerView.hidden = YES;
    [self.view addSubview:self.gAdBannerView];
}

- (void)createAndLoadInterstitial {
    gInterstitialView = [[GADInterstitial alloc] init];
    gInterstitialView.adUnitID = @"ca-app-pub-1561400000155036/7934798102";
    gInterstitialView.delegate = self;
    
    GADRequest *request = [GADRequest request];
    // Request test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made. GADInterstitial automatically returns test ads when running on a
    // simulator.
    //    request.testDevices = @[
    //                            @"2077ef9a63d2b398840261c8221a0c9a"  // Eric's iPod Touch
    //                            ];
    [gInterstitialView loadRequest:request];
}


// Hide the banner by sliding down
-(void)hideBanner:(UIView*)banner
{
    if (banner && ![banner isHidden])
    {
        [UIView beginAnimations:@"hideBanner" context:nil];
        banner.frame = CGRectOffset(banner.frame, 0, banner.frame.size.height);
        [UIView commitAnimations];
        banner.hidden = TRUE;
    }
}

// Show the banner by sliding up
-(void)showBanner:(UIView*)banner
{
    if (banner && [banner isHidden])
    {
        [UIView beginAnimations:@"showBanner" context:nil];
        banner.frame = CGRectOffset(banner.frame, 0, -banner.frame.size.height);
        [UIView commitAnimations];
        banner.hidden = FALSE;
    }
}

// Called before the add is shown, time to move the view
- (void)bannerViewWillLoadAd:(ADBannerView *)banner
{
    NSLog(@"iAd load");
    [self hideBanner:self.gAdBannerView];
    [self showBanner:self.iAdBannerView];
}

// Called when an error occured
- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"iAd error: %@", error);
    [self hideBanner:self.iAdBannerView];
    [self.gAdBannerView loadRequest:[GADRequest request]];
}

// Called before ad is shown, good time to show the add
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    NSLog(@"Admob load");
    //[self hideBanner:self.iAdBannerView];
    [self showBanner:self.gAdBannerView];
}

// An error occured
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Admob error: %@", error);
    [self hideBanner:self.gAdBannerView];
}

// Rotate the photo taken from Camera when filtering it ============
- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

- (void)interstitial:(GADInterstitial *)interstitial
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitialDidFailToReceiveAdWithError: %@", [error localizedDescription]);
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    NSLog(@"interstitial Ad");
    
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    NSLog(@"interstitialDidDismissScreen");
    [self createAndLoadInterstitial];
    
    if(isShareInstagram)
    {
        [self processShareToInstagram];
    }
    else
    {
        [self processShareToSave];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
