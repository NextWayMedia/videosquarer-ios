//
//  AppDelegate.h
//  instaFix
//
//  Created by MacBook FV iMAGINATION on 21/07/14.
//  Copyright (c) 2014 FV iMAGINATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
