//
//  UIPurchaseView.m
//  VideoSquarer
//
//  Created by Artem Volin on 3/18/15.
//  Copyright (c) 2015 FV iMAGINATION. All rights reserved.
//

#import "UIPurchaseView.h"

@implementation UIPurchaseView
{
    
    __weak IBOutlet UIView *contentView;
    __weak IBOutlet UIButton *buyButton;
    __weak IBOutlet UIButton *otherFeaturesButton;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

+ (id)instantation
{
    UIPurchaseView *view = [[[NSBundle mainBundle] loadNibNamed:@"purchaseview" owner:nil options:nil] objectAtIndex:0];
    [view initView];
    return view;
}

- (void)initView
{
    otherFeaturesButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    // you probably want to center it
    otherFeaturesButton.titleLabel.textAlignment = NSTextAlignmentCenter; // if you want to
//    [otherFeaturesButton setTitle: @"OTHER\nPREMIUM FEATURES" forState: UIControlStateNormal];
    
    buyButton.layer.cornerRadius = 5;
    buyButton.layer.masksToBounds = YES;
    
    contentView.layer.cornerRadius = 5;
    contentView.layer.masksToBounds = YES;
}

- (void) dismissView
{
    CGAffineTransform baseTransform = self.transform; //1
    baseTransform = CGAffineTransformTranslate(baseTransform,0, self.frame.size.height); //2
    
    [UIView animateWithDuration: 0.5 animations:^{
        self.transform = baseTransform; //3
    } completion:^(BOOL finished) {
        if(finished)
            [self removeFromSuperview];
    }];

}
- (IBAction)onTouchPurchase:(id)sender {
    [_controller purchaseBlur];
    [self dismissView];
}

- (IBAction)onTouchOtherFeatures:(id)sender {
    [self dismissView];

}


- (IBAction)onToutchLater:(id)sender {
    [self dismissView];
}

@end
